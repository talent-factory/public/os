### Konfiguration der IDE

Damit der _Code_ unter verschiedenen Entwicklern identisch formatiert wird stellen wir die 
IDE wie folgt ein:

- Set from... (`Google` auswählen)
- Indent: `4`

![image](./doc/images/code-style.png)