\subsection*{Übungen}

\begin{Exercise}[label=ps,title={Aktuelle Prozessliste}]

    \cmd{ps} ist ein Befehl, der Informationen über alle derzeit in Ihrem System laufenden
    Prozesse anzeigt. Lesen Sie die \emph{Manpage} des Befehls \texttt{ps}. Geben Sie die
    folgenden Befehle ein: (1) \cmd{ps -ef | more} und (2) \cmd{ps -aux | more}. Mit diesen beiden
    Befehlen wird eine lange Liste von Prozessen angezeigt. Stellen Sie fest, welche Prozesse
    beim Hochfahren des Systems gestartet werden und welche Prozesse später gestartet werden.
    Finden Sie für jeden Prozess heraus, wem er gehört, welchen Code er ausführt und wie viel
    CPU/Speicher er verbraucht hat.

    Speichern Sie nun die Details aller Prozesse, die root gehören, in einer Datei namens
    \texttt{root-processes-1}, und alle Prozesse, die Ihnen gehören, in einer Datei namens
    \texttt{my-processes-1}. Starten Sie nun Ihr System neu und erstellen Sie ähnliche
    Dateien, \texttt{root-processes-2} und \texttt{my-processes-2}. Vergleichen Sie
    die jeweiligen Paare und erläutern Sie die Unterschiede zwischen den beiden Dateien.

\end{Exercise}

% Referenzen:
% - https://www.thegeekstuff.com/2012/04/create-threads-in-linux/
% - https://www.cs.cmu.edu/afs/cs/academic/class/15492-f07/www/pthreads.html
% - https://stackoverflow.com/questions/36905017/how-compile-with-minix-mthread-h-in-minix
% - https://github.com/0xffea/MINIX3/blob/master/include/minix/mthread.h
% - http://homepages.cs.ncl.ac.uk/nick.cook/csc2025/minix/3.2.1/usr/src/include/minix/mthread.h
% - https://openbook.rheinwerk-verlag.de/linux_unix_programmierung/Kap10-004.htm
% - https://www.uni-muenster.de/AMM/num/Vorlesungen/WissenschaftlichesRechnen_SS11/pthreads.pdf

\begin{Exercise}[label=parent-child,title={Erstellen eine Eltern- und Kindprozesse}]

    Schreiben Sie ein C-Programm, das einen Kindprozess erstellt. Der Kindprozess
    soll eine Nachricht ausgeben, und der Elternprozess soll warten,
    bis der Kindprozess beendet ist, bevor er ebenfalls eine Nachricht ausgibt.

    Der folgende Code kann als Vorlage verwendet werden:
    \vskip1em
    \src{c}{src/exercise/}{parent-child.c}

\end{Exercise}

\begin{Answer}
    \src{c}{src/solution/}{parent-child.c}
\end{Answer}


\begin{Exercise}[label=threads,title={Verwenden von Threads}]

    Schreiben Sie ein C-Programm, das zwei Threads erstellt. Jeder Thread soll
    eine einfache Nachricht ausgeben. Verwenden Sie die \texttt{pthread}-Bibliothek.

    Der folgende Code kann als Vorlage verwendet werden:
    \vskip1em
    \src{c}{src/exercise/}{threads.c}

\end{Exercise}

\begin{Answer}
    \src{c}{src/solution/}{threads.c}
\end{Answer}


\begin{Exercise}[label=mutex,title={Synchronisation von Threads mit Mutex}]

    Schreiben Sie ein C-Programm, das zwei Threads verwendet, um den Wert
    einer gemeinsamen Variablen zu erhöhen. Verwenden Sie einen Mutex,
    um den Zugriff auf die Variable zu synchronisieren.

    Der folgende Code kann als Vorlage verwendet werden:
    \vskip1em
    \src{c}{src/exercise/}{mutex.c}

\end{Exercise}

\begin{Answer}
    \src{c}{src/solution/}{mutex.c}
\end{Answer}


\begin{Exercise}[label=pipes,title={Prozesskommunikation mit Pipes}]

    Schreiben Sie ein C-Programm, das eine Pipe verwendet, um Daten vom Elternprozess
    an den Kindprozess zu senden. Der Elternprozess soll eine Nachricht in die
    Pipe schreiben, und der Kindprozess soll diese Nachricht lesen und ausgeben.

    Der folgende Code kann als Vorlage verwendet werden:
    \vskip1em
    \src{c}{src/exercise/}{pipe.c}

\end{Exercise}

\begin{Answer}
    \src{c}{src/solution/}{pipe.c}
\end{Answer}


% =======

\begin{Exercise}[title={Anwendungsbereich von Threads}]

    Basierend auf dem Beispiel des Webbrowser-Prozesses, der mehrere Bilder gleichzeitig
    anfordert, beschreiben Sie ein \textbf{eigenes} Beispiel, in dem die Verwendung von Threads
    einem Prozess einen signifikanten Vorteil bietet. Berücksichtigen Sie dabei die
    Aspekte der Parallelität und Effizienzsteigerung.

\end{Exercise}

\begin{Answer}

    Ein gutes Beispiel, das die Vorteile der Verwendung von Threads in einem Prozess hervorhebt,
    ist ein \textbf{Video-Rendering-System}. Video-Rendering ist der Prozess, bei dem ein Computer
    rohe Videodaten, einschliesslich mehrerer Videospuren und Audiodaten, in ein finales Videoformat
    umwandelt. Diese Aufgabe erfordert erhebliche Rechenleistung und kann zeitaufwendig sein,
    besonders bei höheren Auflösungen und komplexen Effekten. Die Verwendung von Threads
    kann die Zeit, die zum Rendern eines Videos benötigt wird, erheblich reduzieren und die
    Effizienz des Gesamtsystems verbessern.

    Beim Rendern eines Videos bestehen die Hauptaufgaben aus der Verarbeitung von Bildern 
    (Frame-Rendering), der Anwendung von Videoeffekten \ z.B. Übergänge, Farbkorrekturen), 
    der Synchronisierung mit Audiospuren und dem Schreiben der Ausgabedaten in eine Videodatei. 
    Diese Aufgaben können in mehrere Threads aufgeteilt werden, um Parallelität zu erreichen:
    
    \begin{itemize}
        \item\textbf{Bild-Rendering-Threads} -
        Jeder dieser Threads kann für das Rendering individueller Frames oder Frameblöcke
        zuständig sein. Statt die Frames nacheinander zu rendern, ermöglichen mehrere Threads
        das gleichzeitige Rendering mehrerer Frames. Dies ist besonders nützlich bei Videos
        mit hoher Frame-Rate oder Auflösung, wo das Rendern eines einzelnen Frames
        rechenintensiv sein kann.
        \item\textbf{Effektverarbeitung-Threads} -
        Effekte und Übergänge können parallel angewendet werden, besonders wenn sie unabhängig
        voneinander sind. Es könnte beispielsweise einen Thread für Farbkorrekturen, einen
        für Übergänge und einen für andere Effekte geben.
        \item\textbf{Audio-Verarbeitung-Thread} -
        Während die Videoverarbeitungs-Threads arbeiten, kann ein separater Thread für die 
        Audioverarbeitung eingesetzt werden, um sicherzustellen, dass die Audiospuren korrekt 
        bearbeitet und mit dem Video synchronisiert werden.
        \item\textbf{Encoding- und Schreib-Threads} -
        Ein oder mehrere Threads könnten für das Kodieren der gerenderten Frames und deren
        Schreiben in eine Ausgabedatei verantwortlich sein. Diese Trennung ermöglicht es,
        gerenderte Frames schnell zu kodieren und zu speichern, während gleichzeitig neue
        Frames vorbereitet werden.
    \end{itemize}

\end{Answer}


% =======

\begin{Exercise}[title={Bedeutung von Systemaufrufen}]

    Erklären Sie, was Systemaufrufe sind und welche Rolle sie in der Interaktion zwischen
    einem Betriebssystem und den laufenden Programmen spielen. Beschreiben Sie, wie
    Systemaufrufe zur Realisierung der Abstraktionen beitragen, die ein Betriebssystem
    bereitstellt, und reflektieren Sie, warum diese für die effiziente Programmierung
    wichtig sind. Nutzen Sie, wenn möglich, Beispiele aus Ihrem eigenen Erfahrungen
    oder allgemeinen Vorstellungen zu Computern und Programmierung.

\end{Exercise}

\begin{Answer}

    Systemaufrufe sind die Schnittstellen zwischen einem Programm und dem Betriebssystem,
    durch die Programme Dienste vom Betriebssystem anfordern können. Sie sind essentiell,
    weil sie den Programmen erlauben, Hardware-Ressourcen durch das Betriebssystem
    zu nutzen, ohne direkten Zugriff auf die Hardware zu benötigen. Dies unterstützt
    die Abstraktionen, die das Betriebssystem bereitstellt, indem sie eine
    standardisierte Methode für Programme anbieten, um Dateioperationen
    durchzuführen, mit der Netzwerkschnittstelle zu kommunizieren oder Speicher
    anzufordern. Durch die Benutzung von Systemaufrufen können Entwickler
    sich auf die Anwendungslogik konzentrieren und müssen sich weniger mit
    den komplexen Details der Hardware-Interaktion beschäftigen.

\end{Answer}


% =======

\begin{Exercise}[title={Thread- und Prozessattribute}]

    In einem Betriebssystem haben sowohl Prozesse als auch Threads ihren eigenen Satz
    von Attributen, die zum Teil unterschiedlich sind. Ohne direkten Bezug auf
    das Skript zu nehmen, erklären Sie die Rolle des Programmzählers und des
    Stacks in einem Thread und wie diese Attribute dazu beitragen,
    die Unabhängigkeit und den Kontext eines Threads innerhalb eines
    Prozesses zu gewährleisten.

\end{Exercise}

\begin{Answer}

    In der Welt der Betriebssysteme sind sowohl Prozesse als auch Threads
    Schlüsselkomponenten für Multitasking und parallele Ausführung von Aufgaben.
    Während Prozesse als unabhängige Ausführungseinheiten fungieren,
    die über ihren eigenen Adressraum, globale Variablen und Systemressourcen
    verfügen, sind Threads leichtgewichtige Ausführungseinheiten innerhalb
    eines Prozesses, die sich den Adressraum und die Ressourcen des Prozesses
    teilen, jedoch eigene unabhängige Ausführungswege haben können.
    Zwei wichtige Attribute, die eine Schlüsselrolle in der Unabhängigkeit
    und dem Kontext eines Threads spielen, sind der Programmzähler
    (Program Counter, PC) und der Stack.

    \begin{itemize}
        \item\textbf{Programmzähler (Program Counter, PC)}\newline
        Der Programmzähler ist ein spezielles Register in der CPU, das die 
        Adresse des nächsten auszuführenden Maschinensprachebefehls in einem 
        Programm enthält. In einem multithreaded Szenario besitzt jeder 
        Thread seinen eigenen Programmzähler. Dies bedeutet, dass jeder 
        Thread unabhängig voneinander den nächsten Befehl ausführen kann, 
        der auf ihn wartet, unabhängig davon, an welcher Stelle andere 
        Threads sich im Programmcode befinden. Der Programmzähler ist 
        entscheidend, um die Ausführung eines Threads zu steuern und 
        sicherzustellen, dass bei einem Kontextwechsel (d.h., wenn der 
        Scheduler von einem Thread zu einem anderen wechselt) der 
        aktuelle Ausführungsstand jedes Threads korrekt gespeichert und 
        später wiederhergestellt werden kann. Somit trägt der 
        Programmzähler massgeblich zur Unabhängigkeit eines Threads bei,
        indem er diesem ermöglicht, an dem genauen Punkt fortzufahren, 
        wo er unterbrochen wurde.

        \item\textbf{Stack}\newline
        Während sich Threads den Heap-Speicher und globale Variablen mit
        anderen Threads desselben Prozesses teilen, verfügt jeder Thread
        über seinen eigenen Stack. Dieser Stack enthält lokale Variablen,
        Funktionsparameter und Rückgabeadressen von Funktionen oder
        Methodenaufrufen, die innerhalb des Threads ausgeführt werden.
        Der individuelle Stack eines Threads ist von entscheidender
        Bedeutung für die Handhabung von Funktionsaufrufen und die
        Rückkehr aus diesen, da er den Zustand aller Funktionen,
        die innerhalb des Threads ausgeführt werden, aufrechterhält.
        Er ermöglicht die Unabhängigkeit von Threads, indem er sicherstellt,
        dass die lokale Ausführungsumgebung eines Threads (z.B.\ lokale
        Variablen und Rücksprungadressen) isoliert und unbeeinträchtigt
        vom Zustand anderer Threads bleibt.

    \end{itemize}

    \textbf{Beitrag zur Unabhängigkeit und zum Kontext}

    Zusammen tragen der Programmzähler und der Stack wesentlich zur
    Unabhängigkeit und zum Kontext eines Threads bei einem Multithreading-Prozess
    bei. Der Programmzähler sorgt dafür, dass jeder Thread genau weiss,
    an welcher Stelle im Code er fortfahren soll, während der individuelle
    Stack jedes Threads eine isolierte Umgebung für lokale Ausführungen
    bietet. Diese Attribute ermöglichen es Threads, quasi gleichzeitig
    und unabhängig voneinander Aufgaben auszuführen und dabei den Kontextwechsel
    effizient zu handhaben. Das Ergebnis ist eine verbesserte
    Ausführungsgeschwindigkeit und die Fähigkeit eines Programms,
    parallel verschiedene Teilaufgaben zu bearbeiten, ohne dass die
    Ausführungsstränge einander stören.

\end{Answer}