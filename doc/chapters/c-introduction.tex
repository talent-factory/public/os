\section{Eine Einführung in C}
\label{sec:c-introduction}

Für unseren Kurs benötigen wir nur ein Grundwissen der Programmiersprache C, damit wir
die angefügten Codebeispiele aus dem Betriebssystem MINIX lesen und verstehen können. Ein
etwas weiterführender Kurs steht unter~\cite{c:grundkurs} zur Verfügung.

Wir schreiben ein erstes Progrmm zur Veranschaulichung der Syntax. Hierzu wollen wir einen Text auf der
Kommandozeile ausgeben. Beliebige Texte können in C in Anfuhrungszeichen eingeschlossen geschrieben werden.
Es gibt eine eingebaute Funktion namens \cmd{printf}, der solche Texte übergeben werden können,
damit sie die Texte auf der Kommandozeile ausgibt. Um diese Standardfunktion benutzen zu können,
muss eine entsprechende Bibliothek für standard Ein- und Ausgabe, in das Programm mit eingebunden werden.

\src{c}{src/examples/}{hello.c}

Bevor wir hier weiter eintauchen wollen wir uns einige Grundkonzepte anschauen. C ist wie viele
andere Sprachen (z.B.\ Java) eine \href{https://de.wikipedia.org/wiki/Imperative_Programmierung}{imperative}
Sprache mit Datentypen, Variablen und Steueranweisungen. Die elementaren Datentypen in C sind
ganze Zahlen, Zeichen und Gleitkommazahlen. Zusammengesetzte Datentypen können durch die Benutzung
von Feldern, Strukturen und Variantenrecords konstruiert werden.

Ein Konzept in C, das Java nicht kennt, sind die expliziten Zeiger. Ein Zeiger (\emph{pointer})
ist eine Variable, die auf eine andere Variable oder Datenstruktur verweist (d.h., sie enthält deren Adresse).
Betrachten wir als Beispiel die folgenden Anweisungen:

\begin{minted}[autogobble]{c}
    char c1, c2, *p;

    c1 = 'c';
    p = &c1;
    c2 = *p;
\end{minted}

Hier werden \texttt{c1} und \texttt{c2} als Zeichenvariablen deklariert und
\texttt{p} als eine Variable, die auf ein Zeichen zeigt (d.h., die Adresse
eines Zeichens enthält). Die erste Zuweisung speichert den ASCII-Code für das
Zeichen \texttt{'c'} in der Variablen \texttt{c1} ab.
Die zweite weist die Adresse von \texttt{c1} der Zeigervariablen \texttt{p}
zu und die dritte den Inhalt der Variable, auf die \texttt{p} zeigt,
der Variablen \texttt{c2} . Nachdem alle Anweisungen ausgeführt sind,
enthält also \texttt{c2} ebenfalls den ASCII-Code für \texttt{'c'}.

\subsection*{Header-Dateien}

Ein Betriebssystemprojekt besteht im Allgemeinen aus einer Reihe von
Verzeichnissen mit vielen \texttt{.c}-Dateien, die den Code für einige
Systemteile enthalten, und mit einigen \texttt{.h}-Header-Dateien,
die Deklarationen und Definitionen für eine oder mehrere Code-Dateien
enthalten. Header-Dateien können auch einfache Makros einschliessen,
die es dem Programmierer erlauben, Konstanten zu benennen.

\begin{minted}[autogobble]{c}
    #define BUFFER_SIZE 4096
\end{minted}

So wird \mintinline{c}{BUFFER_SIZE}, wenn es in einem Code auftritt,
während der Übersetzung durch die Zahl $4.096$ ersetzt. Makros können
Parameter haben, zum Beispiel

\begin{minted}[autogobble]{c}
    #define max(a, b) (a > b ? a : b)
\end{minted}

Damit hat die Zuweisung in einer C-Datei

\begin{minted}[autogobble]{c}
    i = max(j, k+1)
\end{minted}

die gleiche Bedeutung wie

\begin{minted}[autogobble]{c}
    i = (j > k + 1 ? j : k + 1)
\end{minted}

d.h., der grössere Wert von $j$ und $k + 1$ wird in $i$ gespeichert.
Header können auch bedingte Übersetzungen enthalten wie in

\src{c}{src/include/}{stdio.h}

In diesem Beispiel wird sichergestellt, dass diese \emph{Header-}Datei nur einmal
importiert wird.


\subsection*{Grössere C-Projekte}

Um ein Betriebssystem zu erstellen, wird jede \texttt{.c}-Datei durch den C-Compiler
in eine Objektdatei übersetzt. Objektdateien, zu erkennen an der Endung
\texttt{.o}, enthalten binäre Befehle für die Zielmaschine. Sie werden später direkt
von der CPU ausgeführt.

Der erste Durchlauf des C-Compilers heisst C-Präprozessor. Er liest jede \texttt{.c}-Datei
ein und holt immer, sobald er auf eine \texttt{\#include}-Direktive trifft, die dort
genannte Header-Datei und verarbeitet sie. Ausserdem expandiert er Makros, behandelt
bedingte Übersetzungen und übergibt schliesslich die Ergebnisse dem nächsten Durchlauf
des Compilers, als ob sie in der ursprünglichen \texttt{.c}-Datei enthalten wären.

Da Betriebssysteme sehr gross sind (> 1'000'000 Codezeilen sind nicht ungewöhnlich),
wäre die vollständige Neuübersetzung nach jeder Änderung in einer Datei untragbar.
Um den Überblick darüber zu behalten, welche Objektdatei von welchen Header-Dateien
abhängt, wird im sogenannten \emph{Makefile} beschrieben.

Hier ein einfacheres Beispiel einer \emph{Makefile}-Datei, welche zur Erstellung
der vorliegenden Dokumentes verwendet wird.

\src{Makefile}{doc/}{Makefile}


Sind einmal sämtliche Abhängigkeiten definiert, dann kann das ganze
Projekt (z.B.\ Betriebssystem, Dokumentation,$\ldots$) mit einem Befehl
neu erstellt werden.

\begin{minted}[autogobble]{sh}
        $ make
\end{minted}

Die Aufgabe von \cmd{make} ist es nun herauszufinden, welche Objektdateien
erforderlich sind, um die Binärdatei des Betriebssystems zu erstellen,
die jetzt sofort gebraucht wird. Dann prüft make für jede Datei, ob eine
der Dateien, von denen die Binärdatei abhängt (der Code oder Header),
seit der letzten Übersetzung verändert wurde. Falls ja, dann muss diese
Objektdatei neu übersetzt werden.

\image{01-30}{Der Übersetzungsprozess von C- und Header-Dateien}

Wenn make festgelegt hat, welche \texttt{.c}-Dateien neu übersetzt werden
müssen, ruft es dazu den C-Compiler auf. Somit reduziert sich die Anzahl
der Übersetzungen auf das absolute Minimum. Wenn alle \texttt{.o}-Dateien
bereitstehen, werden sie dem Binder (\emph{linker}) übergeben, einem Programm,
das alle \texttt{.o}-Dateien zu einer einzigen ausführbaren binären Datei
kombiniert. Zu diesem Zeitpunkt werden auch alle aufgerufenen Bibliotheksfunktionen
hinzugenommen, die Verweise zwischen Funktionen werden aufgelöst und die
Maschinenadressen werden nötigenfalls neu berechnet.

Sobald der Binder seine Aufgabe beendet hat, ist das Ergebnis ein ausführbares
Programm, das in UNIX-Systemen traditionell mit \texttt{a.out} bezeichnet wird.
Die verschiedenen Phasen dieses Prozesses bei einem Programm mit drei C-Dateien
und zwei Header-Dateien sind in \imageref{fig:01-30} zu sehen.


\subsection*{Grundlegende Konzepte}

Ein C-Programm, also ein C-Quelltext, besteht aus den Zeichen des
ASCII-Zeichensatzes (Tabelle~\ref{tab:ascii}, Seite~\pageref{tab:ascii}),
der mit ganzen Zahlen von 0 bis 127 kodiert wird. So eine Zahl kann binär
mit 7 Bits dargestellt werden. Den ASCII-Zeichensatz teilt man in zwei Gruppen
von Zeichen ein: nicht druckbare Zeichen (mit ASCII-Codes kleiner als 32) und
druckbare Zeichen (die Codes grösser als 32 haben).

\subsubsection*{Namen, Bezeichner}

Ein Bezeichner (Name) muss immer mit einem Buchstaben oder mit einem ’\texttt{\_}’
(Unterstrich) beginnen, dann können sich Buchstaben, Unterstriche und auch
Ziffern in beliebiger Reihenfolge abwechseln. Gross- und Kleinbuchstaben werden
unterschieden, d.h.\ max, Max und MAX sind drei verschiedene Bezeichner.
Man verwendet sie für Variablen, Konstanten, Typnamen, Funktionen, Namen von
Strukturen, Unions, Bitfeldern, Aufzählungstypen, Makros und Makroparametern.

\subsubsection*{Schlüsselwörter}

Schlüsselwörter (reservierte Wörter) sind für den C-Compiler von einer besonderen
Bedeutung: Es gibt sie nur für Anweisungen, grundlegende Datentypen und für die
Definitionssyntax für Funktionen und Datentypen.
In \href{https://en.wikipedia.org/wiki/ANSI_C}{ANSI C}
(\emph{American National Standards Institute}) gibt es die folgenden 32 reservierten
Wörter, die immer klein geschrieben werden:

\begin{table}[!ht]
    \begin{tabularx}{\textwidth}{p{0.22\textwidth}p{0.22\textwidth}p{0.22\textwidth}X}
        \midrule
        \texttt{auto}     & \texttt{double} & \texttt{int}      & \texttt{struct}   \\ \midrule
        \texttt{break}    & \texttt{else}   & \texttt{long}     & \texttt{switch}   \\ \midrule
        \texttt{case}     & \texttt{enum}   & \texttt{register} & \texttt{typedef}  \\ \midrule
        \texttt{char}     & \texttt{extern} & \texttt{return}   & \texttt{union}    \\ \midrule
        \texttt{const}    & \texttt{float}  & \texttt{short}    & \texttt{unsigned} \\ \midrule
        \texttt{continue} & \texttt{for}    & \texttt{signed}   & \texttt{void}     \\ \midrule
        \texttt{default}  & \texttt{goto}   & \texttt{sizeof}   & \texttt{volatile} \\ \midrule
        \texttt{do}       & \texttt{if}     & \texttt{static}   & \texttt{while}    \\ \midrule
    \end{tabularx}
    \caption{Schlüsselwörter}
    \label{tab:keywords}
\end{table}


\subsubsection{Elementare Datentypen}

Unter Datentyp versteht man eine Menge, in der die folgenden Begriffe
festgestellt sind:

\begin{itemize}
    \item Speichergröße und Lebensdauer jedes Elements.
    \item Eine Menge von Operationen, mit deren Hilfe die Werte dieses Typs modifiziert
        und überarbeitet werden können und die Bedeutung dieser Operationen.
    \item Die zugelassenen Operatoren und Einschränkungen, die für sie gelten.
\end{itemize}

Datentypen können elementar (vordefiniert) oder benutzerdefiniert (abgeleitet) sein.
Die elementare Datentypen und deren Grössen:

\begin{table}[!ht]
    \begin{tabularx}{\textwidth}{p{0.17\textwidth}p{0.4\textwidth}p{0.25\textwidth}X}
        \headcell{Typ} & \headcell{Beschreibung} & \headcell{Gültigkeitsbereich} & \headcell{Bytes}             \\
        \addlinespace
        \texttt{char, \newline unsigned char}  & Zeichen durch einen ASCII-Code dargestellt                      & $[-128, 127]$     \newline $[0, 255]$   & 1 \\ \midrule
        \texttt{short,\newline unsigned short} & Binäre ganze Zahl durch das Komplementieren gegen 2 dargestellt & $[-32768, 32767]$ \newline $[0, 65535]$ & 2 \\ \midrule
        \texttt{int,  \newline unsigned int}   & Binäre ganze Zahl durch das Komplementieren gegen 2 dargestellt & Platform-/ \newline Compilerabhängig    & 2, 4, 8 \\ \midrule
        \texttt{long, \newline unsigned long}  & Binäre ganze Zahl durch das Komplementieren gegen 2 dargestellt & $[-2^{31}, 2^{31}-1]$ \newline $[0, 2^{32}-1]$ & 4, 8 \\ \midrule
        \texttt{float}                         & Einfach genaue Flieskommazahl                                   & $[1.7 * 10^{-308}, 1.7 * 10^{308}]$ \newline (im Betrag) & 4    \\ \midrule
        \texttt{double}                        & Doppelt genaue Flieskommazahl                                   & $[2.3 * 10^{-308}, 1.7 * 10^{308}]$ \newline (im Betrag) & 8    \\ \midrule
        \texttt{long double}                   & Doppelt genaue Flieskommazahl                                   & $[3.4 * 10^{-4932}, 1.1 * 10^{4932}]$ \newline (im Betrag) & 8    \\ \midrule
    \end{tabularx}
    \caption{Standard-Datentypen in C}
    \label{tab:data-types}
\end{table}

\clearpage
\subsection*{Übungen}

Versuche die folgenden Übungen mithilfe von Google, ChatGPT oder anderen
\emph{online} Unterstützungen zu lösen. Die Lösungen zu den einzelnen
Übungen sind auf Seite~\pageref{sec:solution} zu finden.

% https://www.informatik.htw-dresden.de/~bruns/prog1.html
\begin{Exercise}[label=printsum, title={Ausgeben der Summe, der Differenz und des Quotienten}]

    Schreibe ein Programm, das nacheinander zur Eingabe von 2
    Integerzahlen auffordert, die eingegebenen Zahlen auf zwei
    Variablen vom Typ \texttt{int} abspeichert und danach die Summe, die Differenz,
    das Produkt und den Quotienten ausgibt.

\end{Exercise}

\begin{Answer}

    Der folgende Quelltext liest die Werte zur Aufgabe Seite~\pageref{printsum}
    von der Konsole ein und berechnet anschliessend die geforderten Werte für die
    Summe, die Differenz, das Produkt und den Quotienten.\vskip1em

    \src{c}{src/examples/}{printsum.c}

\end{Answer}


\begin{Exercise}[label=sqrt,title={Berechnen der Quadratwurzel}]

    Erstelle ein Programm, das eine Integerzahl einliest und dann sowohl
    von dieser als auch von ihren 4 direkten Nachfolgern die Quadratwurzel
    berechnet und zusammen mit der Zahl in einer kleinen Tabelle ausgibt.
    Verwende zur Berechnung die Funktion \texttt{sqrt()} der
    Standardbibliothek \texttt{<math.h>}.

\end{Exercise}

\begin{Answer}

    Um ein C-Programm zu erstellen, das eine eingegebene Integerzahl
    und die Quadratwurzeln dieser Zahl sowie ihrer vier direkten Nachfolger
    berechnet und ausgibt, musst du die \texttt{<math.h>} Bibliothek einbinden.
    Hier ist ein Beispiel, wie du das machen kannst:\vskip1em

    \src{c}{src/examples/}{sqrt.c}

    Was macht dieses Programm?
    \begin{enumerate}
        \item Es bindet die Standardbibliotheken \texttt{stdio.h} für Ein-/Ausgabeoperationen
            und \texttt{math.h} für mathematische Funktionen, einschliesslich \texttt{sqrt()}
            für die Quadratwurzelberechnung, ein.
        \item Es fordert den Benutzer auf, eine Integerzahl einzugeben und speichert diese in
            der Variablen \texttt{zahl}.
        \item Es gibt eine Kopfzeile für eine kleine Tabelle aus, die die Spalten für die Zahl
            und ihre Quadratwurzel beschriftet.
        \item Mit einer \texttt{for}-Schleife durchläuft es die Zahl und ihre vier direkten
            Nachfolger, berechnet deren Quadratwurzeln mit \texttt{sqrt()}, und gibt diese Werte
            in der Tabelle formatiert aus. Dabei wird \texttt{printf} genutzt, um die Ausgabe
            zu formatieren – \texttt{\%5d} reserviert 5 Zeichenbreiten für die Integerzahl
            und \texttt{\%12.2f} reserviert 12 Zeichenbreiten für die Quadratwurzel.
    \end{enumerate}

    Beachte, dass für die Verwendung von \texttt{sqrt()} die Kompilierung des Programms mit
    einem Mathematik-Flag erforderlich sein kann, z.~B\@.

    \begin{minted}[autogobble]{c}
        $ gcc sqrt.c -o sqrt -lm
        $ ./sqrt
        Bitte geben Sie eine Integerzahl ein: 10

         Zahl | Quadratwurzel
        --------------------
           10 |         3.16
           11 |         3.32
           12 |         3.46
           13 |         3.61
           14 |         3.74
    \end{minted}

\end{Answer}


\begin{Exercise}[label=conversion,title={Typumwandlung einer Gleitkommazahl}]

    Dividiere zwei Integerzahlen, die von der Tastatur eingelesen wird,
    einmal mit und einmal ohne erzwungene Typumwandlung zur Gleitkommazahl
    und speichere das Ergebnis jeweils auf eine Variable vom Typ \texttt{float}.
    Gib das Ergebnis aus und erkläre die Abweichung der beiden Werte,
    wenn die eine Integerzahl nicht durch die andere teilbar ist.

\end{Exercise}

\begin{Answer}

    Das Programm liest zwei Integerzahlen ein, führt einmal eine Division
    mit und einmal ohne erzwungene Typumwandlung (\emph{Casting}) zur Gleitkommazahl
    durch, speichert die Ergebnisse in \texttt{float} Variablen und gibt diese aus.\vskip1em

    \src{c}{src/examples/}{sqrt.c}

    Die Abweichung entsteht, weil bei der Division ohne erzwungene Typumwandlung
    das Ergebnis als Integer berechnet wird, was bedeutet, dass der Dezimalteil
    verloren geht. Erst nach dieser Berechnung wird das Ergebnis in eine
    Gleitkommazahl konvertiert, ohne den ursprünglichen Dezimalteil.
    Bei der Division mit erzwungener Typumwandlung wird die Division
    im Gleitkommabereich durchgeführt, wodurch der Dezimalteil erhalten bleibt.

\end{Answer}


\begin{Exercise}[label=dice,title={Berechnungen am Würfel}]

    Erstelle ein Programm zur Berechnung von Eigenschaften eines Würfels,
    das bei gegebener (d.h.\ in unserem Fall einzugebender) Kantenlänge $a$
    den Flächenumfang ($6 a \times a$), das Volumen ($a^{3}$) und die Länge der
    Raumdiagonale ($a \sqrt{3})$) berechnet und ausgibt.

\end{Exercise}

\begin{Answer}

    Um die gewünschte Funktionalität zu implementieren, kannst du folgendes
    C-Programm verwenden. Es berechnet den Flächenumfang, das Volumen und die Länge
    der Raumdiagonale eines Würfels basierend auf der eingegebenen Kantenlänge $a$.\vskip1em

    \src{c}{src/examples/}{dice.c}

\end{Answer}


\begin{Exercise}[label=values,title={Arithmetischen Ausdrücke}]

    Welchen Wert haben die folgenden arithmetischen Ausdrücke?

    \begin{minted}[autogobble,frame=single]{shell}
        3/4   15%4   15/2.0   3+5%4   3*7%4
    \end{minted}

\end{Exercise}


\begin{Exercise}[label=priorities,title={Operanden und Operatoren}]

    Wie werden die Operanden und Operatoren im folgenden Ausdruck zusammengefasst?

    \begin{minted}[autogobble,frame=single]{c}
        x = -4 * ++i - 6 % 4
    \end{minted}

\end{Exercise}
