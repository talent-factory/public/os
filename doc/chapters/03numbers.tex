%! suppress = TooLargeSection
\section{Zahlensysteme}
\label{sec:zahlensysteme}

In unserer Umgangssprache werden die Begriffe Nachricht, Daten und Information
häufig synonym verwendet. Es gibt jedoch Unterschiede: Information ist ein
abstrakter Begriff, der das Wissen über ein Ereignis oder einen Tatbestand
bezeichnet, während Daten und Nachricht die äussere Form, die Darstellung der
Information, charakterisieren. Man könnte auch sagen: Daten sind physisch auf
einem Datenträger (z.B.~Papier) verfügbar. Information ist der abstrakte Inhalt
von Daten, den der Empfänger durch Interpretation gewinnt.

\subsection{Codierung}
\label{subsec:codierung}

In den üblichen technischen Anwendungen gibt es sowohl analoge als auch digitale
Darstellungen von Information. Eine Darstellung heisst analog, wenn eine Grösse
durch eine Vergleichsgrösse dargestellt wird, etwa die Temperatur der Luft durch
die Länge einer Quecksilbersäule oder die verstrichene Zeit durch den Winkel
eines Zeigers. Zum Ablesen dient dann eine Skala, die bereits in der gesuchten
Einheit beschriftet ist. Eine Darstellung heisst digital, wenn eine Grösse durch
Zahlen oder ganz allgemein symbolisch dargestellt wird.

Für eine maschinelle Bearbeitung von Informationen ist die analoge Darstellung
nicht so gut geeignet wie die digitale. In einem ersten Schritt wollen wir
einige grundlegende Begriffe für die Darstellung von Information in einem
einführen.

Ein \textit{Zeichen} ist ein Element aus einer vereinbarten endlichen Menge
verschiedener Symbole, dem so genannten Zeichenvorrat. Ist die Menge geordnet,
so heisst sie Alphabet. Bekannte Beispiele sind das Buchstabenalphabet
$\{A,B,C,\ldots,Z\}$ und das Ziffernalphabet $\{0,1,2,\ldots,9\}$.

Einen zweielementigen Zeichenvorrat nennt man \textit{binären} Zeichenvorrat.
Die Zeichen darin heissen \textit{Binärzeichen} oder \textit{Bit}. Ein Bit
ist also eine Stelle in einer binären Zeichenfolge. Beispiele für binäre
Alphabete sind etwa $\{ja,nein\}$, $\{true, false\}$ oder $\{1,0\}$.
In einem Computer werden binäre Zeichen durch unterschiedliche physikalische
Zustände der Schaltelemente dargestellt, etwa durch hohe oder niedrige Spannung
${H, L}$. Der interne Zeichenvorrat eines Computers ist also binär.
Daher muss zur Darstellung des externen Zeichenvorrates jedem Zeichen
intern eine Kombination von Binärzeichen zugeordnet werden.

Unter einem \textit{Wort} versteht man eine Zeichenfolge fester Länge, die als
Einheit betrachtet wird. Ein binäres Wort der Länge 8 heisst ein Byte.

Eine Vorschrift zur Abbildung eines Zeichenvorrats in einen anderen Zeichen-
oder Wortvorrat nennt man \textit{Code} oder \textit{Codierung}.
Eine Codierung ist umkehrbar eindeutig, wenn aus der Verschiedenheit der
Bilder die Verschiedenheit der Urbilder folgt und umgekehrt. Nur für einen
umkehrbar eindeutigen Code ist eine Decodierung möglich.

Es gibt mehrere Gründe, warum Codierung notwendig ist:

\begin{description}
    \item[Anpassung an ein Medium.] Ausgehend vom Zeichenvorrat des Mediums
    muss man eine Codierung finden, die nur die verfügbaren Zeichen nutzt.
    Die meisten digitalen Speicher- und Übertragungsmedien verfügen nur
    über ein binäres Alphabet. Also ist es erforderlich, alle Daten binär
    zu codieren, bevor man sie speichern oder übertragen kann.

    \item[Verschlüsselung.] Informationen werden verschlüsselt, um sie vor
    unberechtigtem Zugriff zu schützen.

    \item[Fehlererkennung und -korrektur.] Bei wichtigen Daten ist es nötig,
    spezielle Verfahren zur Codierung zu verwenden, die es erlauben, den
    korrekten Wert zu rekonstruieren, auch wenn ein Teil der Nachricht verändert wurde.

    \item[Kompression.] Es gibt hoch entwickelte Codierungsverfahren, die eine
    Information mit der kleinstmöglichen Anzahl von Bit darstellen können und so
    eine kompakte Speicherung und schnellstmögliche Übertragung der Informationen
    gewährleisten. Ein Beispiel ist etwa die Codierung von Musiksignalen mit dem
    MP3-Verfahren, das Anwendungen wie das Internetradio ermöglicht.
\end{description}

\begin{Example}
    Beim BCD-Code für Dezimalziffern (\emph{Binary Coded Decimals}) werden die
    Dezimalziffern $0$ bis $9$ fortlaufend mit den 4 Bit breiten Kombinationen
    $0000$ bis $1001$ dargestellt. Die nicht verwendeten Werte von
    $1010$ bis $1111$ werden Pseudotetraden genannt.
\end{Example}

\begin{Example}
    Beim $1$-aus-$N$-Code ist in einem $N$ Bit breiten Wort immer genau eine
    Stelle mit einer $1$ besetzt. Der Code dient der Ansteuerung von Schaltungen,
    bei denen aus einer Menge von $N$ Elementen genau eines ausgewählt werden
    soll (Multiplexer, Adressauswahl).
\end{Example}

\begin{table}[!ht]
    \centering
    \begin{tabularx}{0.6\textwidth}{ccc}
        \headcell{Dezimalziffer} & \headcell{BCD-Code} & \headcell{1-aus-10-Code} \\
        \addlinespace
        0                        & 0000                & 0000000001               \\
        1                        & 0001                & 0000000010               \\
        2                        & 0010                & 0000000100               \\
        3                        & 0011                & 0000001000               \\
        4                        & 0100                & 0000010000               \\
        5                        & 0101                & 0000100000               \\
        6                        & 0110                & 0001000000               \\
        7                        & 0111                & 0010000000               \\
        8                        & 1000                & 0100000000               \\
        9                        & 1001                & 1000000000               \\
    \end{tabularx}
    \caption{Beispiele für Codierungen}
    \label{tab:codierung}
\end{table}

\begin{Example}
    Beim ASCII-Code (\emph{American Standard Code for Information Interchange})
    (siehe Tabelle~\ref{tab:ascii}) wird jedem Zeichen (Buchstaben, Ziffern,
    Sonderzeichen) eine 7-Bit-Kombination zugeordnet. Jedes Zeichen kann also in
    einem Byte gespeichert werden. Die Zeichenfolge \texttt{TEXT}
    wird etwa wie folgt codiert:

    \begin{table}[h]
        \centering
        \begin{tabularx}{0.6\textwidth}{l|cccc}
            \textbf{Buchstaben} & T                 & E                 & X                 & T                 \\ \midrule
            \textbf{Binärcode}  & \texttt{01010100} & \texttt{01000101} & \texttt{01011000} & \texttt{01010100} \\
        \end{tabularx}
    \end{table}

\end{Example}

Obwohl der ASCII-Code auch Codes für die Ziffern $0$ bis $9$ enthält,
ist er zur numerischen Verarbeitung von Zahlen nicht besonders geeignet.

Eine Erweiterung des ASCII-Code ist der international genormte ISO-Code.
Die Grundlage bildet der ISO-7-Bit-Code, der mit dem ASCII-Code identisch
ist. Zeichen, bei denen das achte Bit gesetzt ist, sind je nach den
nationalen Erfordernissen definiert. So ist in unserem westlichen
Sprachbereich, einschliesslich Amerika, Australien und Afrika,
die Variante Latein 1 gültig, die die hier gebräuchlichsten Sonderzeichen
erfasst (Tabelle~\ref{tab:ascii}). Man beachte, dass die Umlaute nicht
enthalten sind. Beim Sortieren von Wörtern in lexikalischer Reihenfolge
muss man also Vorkehrungen treffen.

Für bestimmte Anwendungen reicht der Umfang des ASCII-Codes nicht aus.
Daher wurde von einem privaten Konsortium namhafter Computerunternehmer
der so genannte Unicode\footnote{\href{https://www.unicode.org}
{https://www.unicode.org}} entwickelt. Er verwendet 16 Bit zur Darstellung
eines Zeichens und erlaubt damit eine weltweit einheitliche Codierung.


\subsection{Dezimalsystem}
\label{subsec:dezimalsystem}

In der Regel verwenden wir das \textit{arabische Zahlensystem}. Dabei handelt es sich um
ein so genanntes Stellenwertsystem zur Basis zehn. Dass sich gerade die Zahl
zehn als gebräuchlichste Basis entwickelt hat, liegt an unserem natürlichen
\textit{Taschenrechner}, den zehn Fingern. Zahlen werden aus den zehn Ziffern
$\{0,1,2,\ldots,9\}$ gebildet. Einer Ziffer kommt aber, je nach ihrer Stellung
innerhalb der Zahl, jeweils eine andere Wertigkeit zu; sie muss mit einer
entsprechenden Zehnerpotenz (von rechts nach links)
$1 = 10^0$, $10 = 10^1$, $100 = 10^2$, $1000 = 10^3$, $\ldots$ multipliziert
werden.

Grundvoraussetzung für eine solche Darstellung ist die Ziffer $0$, die eine
leere Stelle repräsentiert. Die Ziffer $0$ finden wir schon einige
Jahrtausende v. Chr. in China, wo bereits damals die erste mechanische
Rechenmaschine, der Abakus, in Gebrauch war.

\begin{Example}
    Die Dezimalzahl $1984$ setzt sich zusammen aus den Ziffern $1$, $9$, $8$ und $4$.
    Ihr Wert ergibt sich aus $1\cdot 1000 + 9\cdot 100 + 8\cdot 10 + 4\cdot 1$.

    Allgemein gilt für den Wert $Z$ einer ganzen Dezimalzahl mit den Ziffern
    $Z_i, i = 0, 1, \ldots ,n$:

    \begin{equation}
        Z = \sum_{i=0}^{n} Z_i \cdot 10^i\label{eq:integer}
    \end{equation}

    Diese Zahldarstellung lässt sich bequem für gebrochene Zahlen erweitern.
    Indem man hinter die Einer-Stelle ein Komma schreibt, kann man die Reihe
    der Zehnerpotenzen durch negative Potenzen, also Dezimalbrüche, fortsetzen.

    Also erhalten wir für allgemeine Dezimalbrüche mit $n + 1$ Vorkomma- und $m$
    Nachkommastellen die Formel

    \begin{equation}
        Z = \sum_{i=-m}^{n} Z_i \cdot 10^i\label{eq:decimal}
    \end{equation}

\end{Example}

\begin{Example}
    $129.452 = 2\cdot 10^{-3} + 5\cdot 10^{-2} + 4\cdot 10^{-1} + 9\cdot 10^{0} + 2\cdot 10^{1} + 1\cdot 10^{2}$
\end{Example}

\subsection{Allgemeine Stellenwertsysteme}
\label{subsec:allgemeine-stellenwertsysteme}

Das System der Dezimalzahlen lässt sich auf Zahlensysteme übertragen,
die nicht auf den Potenzen der Zahl 10 aufbauen, sondern auf den Potenzen
einer beliebigen Basis $B$.

Zahlen in einem Stellenwertsystem zur Basis $B\ge 2$ werden mit $B$
verschiedenen Ziffern geschrieben, die die Bedeutung von
$0,1,\ldots$ bis $B - 1$ haben. Entsprechend der Stelle wird der Wert
einer Ziffer dann noch mit einer Potenz von $B$ multipliziert.

Ist also eine Zahl mit den Ziffern $Z_i$ mit $i =-m,\ldots,-1,0,1,\ldots,n$
zur Basis $B$ gegeben, dann erhält man den Wert $Z$ dieser Zahl durch
folgende Summenformel (Potenzformel):

\begin{equation}
    Z = \sum_{i=-m}^{n} Z_i \cdot B^i\label{eq:any-basis}
\end{equation}

Als Beispiele betrachten wir die folgenden in Rechnern üblichen Darstellungen,
die jeweils eine Zweierpotenz als Basis verwenden. Dabei wollen wir uns auf
ganze Zahlen beschränken. Im Dualsystem ist die Basis $B = 2$.

\[
    B = 2; Z_i \in \{0,1\}
\]

Somit gilt:
\[
    10110_2= 0\cdot 2^0 + 1\cdot 2^1 + 1\cdot 2^2 + 0\cdot 2^3 + 1\cdot 2^4 = 22_{10}
\]

Im \textit{Oktalsystem} ist die Basis $B = 2^3 = 8$. Die Ziffern gehen von 0 bis 7.

\[
    B = 8; Z_i \in \{0,1,\ldots,7\}
\]

Somit gilt:
\[
    7603_8 = 3\cdot 8^0 + 0\cdot 8^1 + 6\cdot 8^2 + 7\cdot 8^3 = 3971_{10}
\]

\subsection{Umwandlung der Zahlenformate}
\label{subsec:umwandlung-der-zahlenformate}

Die Umwandlung der Systeme binär, oktal und hexadezimal untereinander erfordert
lediglich eine alternative Betrachtung durch unterschiedliche Gruppierung der
Stellen, da die genannten Systeme alle eine Zweierpotenz als Basis benutzen.

Die Wandlung von einem beliebigen System in das Dezimalsystem erfolgt durch
Auswertung der entsprechenden Summenformel (\ref{eq:any-basis}) oder des
folgenden so genannten Horner-Schemas\footnote{%
    Das \href{https://de.wikipedia.org/wiki/Horner-Schema}{Horner-Schema}
    (nach William George Horner).}:

\begin{equation}
    Z = (( \ldots (( Z_n \cdot B) + Z_{n-1}) \cdot B) + \ldots + z_i) \cdot B + Z_0
    \label{eq:horner}
\end{equation}

Summenformel: \
\[
    5651_8 = 1 + 5\cdot 8^1 + 6\cdot 8^2 + 5\cdot 8^3 = 2985_{10}
\]

Horner Schema:
\[
    5651_8 = ((( 5\cdot 8) + 6) \cdot 8 + 5) \cdot 8 + 1 = 2985_{10}
\]

Für die Umwandlung einer Dezimalzahl in ein System mit anderer Basis gibt es
zwei verschiedene Möglichkeiten:

Das \textit{Subtraktionsverfahren} beruht auf der Darstellung einer Zahl mit
der Summenformel (\ref{eq:any-basis}). Man bestimmt (durch Probieren) die
höchste enthaltene Potenz der Basis $B_j$ und die entsprechende Ziffer $Z_j$
und subtrahiert das Produkt:

\begin{equation}
    Z' = \sum_{i=m}{n} Z_i \cdot B^i - Z_j \cdot B^j\label{eq:subtraktion}
\end{equation}

Dieser Schritt wird nun mit $Z'$ so lange wiederholt, bis man bei $B_0$
angelangt ist. Alle Ziffern, für die keine Subtraktion durchgeführt werden
konnte, werden auf 0 gesetzt. Die Ziffern fallen hier in absteigender
Reihenfolge an.

Das \textit{Divisionsverfahren} bildet das Gegenstück zum Horner-Schema.
Die Zahl $Z$ wird fortlaufend durch die Basis dividiert. Der verbleibende
Rest ergibt die aktuelle Ziffer, und zwar beginnend bei der niederwertigsten
Stelle.

Als Beispiel wandeln wir die Dezimalzahl $45054_{10}$ ins Dualsystem um, indem
wir durch die Basis 2 dividieren und die anfallenden Reste abspalten,
wobei wir als Erstes die niederwertigste Ziffer erhalten:

\[
    \begin{array}{rrccl}
        45054 \div 2 = & 22527  & \mbox{Rest} & 0 & = Z_0 \\
        22527 \div 2 = & 11263  & \mbox{Rest} & 1 & = Z_1 \\
        11263 \div 2 = & 5631   & \mbox{Rest} & 1 & = Z_2 \\
        & \cdots &             &   &       \\
        1 \div 2 =     & 0      & \mbox{Rest} & 1 & = Z_n \\
    \end{array}
\]

Ergibt insgesamt $45054_{10} = 1010111111111110_2$.

\subsection{Negative Zahlen}
\label{subsec:negative-zahlen}

Bei der Darstellung negativer Zahlen unterscheidet man zwischen
\textit{Vorzeichen-Betrags-Darstellung} und \textit{Komplementdarstellung}.

Die Vorzeichen-Betrags-Darstellung entspricht unserem gewohnten Umgang mit
Zahlen beim Rechnen mit Bleistift und Papier. Nehmen wir an, wir hätten $n$
Stellen, um eine Zahl darzustellen, so gibt die höchstwertige Stelle
das Vorzeichen an, die restlichen $n - 1$ Stellen enthalten den
Absolutbetrag $\lvert Z \rvert$ der Zahl.
In der binären Darstellung entspricht '$0$' dem '+' und '$1$' dem '-' Zeichen.

Mit $n$ Binärstellen lässt sich damit der Wertebereich

\[
    - (2^{n-1} -1) \le Z \le 2^{n-1} -1
\]

darstellen, wobei die Null dabei zwei Darstellungen ($-0,+0$) hat.
Für $n=8$ erhalten wir:

\[
    %! suppress = MathOperatorEscape
    \begin{array}{lcrrcr}
        X_{\mbox{\tiny max}} & = & 01111111 = & (2^7 - 1) & = & 127     \\
        X_{\mbox{\tiny min}} & = & 11111111 = & -(2^7 - 1) & = & -127
    \end{array}
\]

Diese Darstellung ist einfach, hat aber mehrere Nachteile. Zum einen ist es
unschön zwei verschiedene Darstellungen für dieselbe Zahl Null zu haben, noch
schlechter aber ist es, dass man für Addition und Subtraktion verschiedene
Verfahren benötigt. Es müssen daher vor der Durchführung dieser Operationen die
Vorzeichen der Eingangsoperanden getestet werden.

Die Komplementdarstellung kann diese Nachteile vermeiden und hat sich daher
allgemein durchgesetzt. Man unterscheidet zwei Arten von Komplementen, nämlich
das \textit{Stellenkomplement} (Eins-Komplement) und das echte Komplement
(Zwei-Komplement).

\image{zahlenkreis}{Der Zahlenkreis für $n=4$}

Das Vorgehen bei der Addition von Komplementzahlen lässt sich am so genannten
Zahlenkreis veranschaulichen (siehe Abbildung~\ref{fig:zahlenkreis}).
Dabei werden die $n$-stelligen vorzeichenlosen Dualzahlen im Uhrzeigersinn auf
einem Kreis angeordnet. Der Übergang von einer Zahl zur nächsten
(im Uhrzeigersinn) erfolgt jeweils durch Addition von 1. Diese Zahlen sind in
dezimaler Schreibweise innerhalb des Ringes angegeben. Der Übergang von
einer Zahl zur vorhergehenden (gegen den Uhrzeigersinn) erfolgt durch
Subtraktion von 1.

Interessant ist die Nahtstelle zwischen der grössten Zahl und der Null.
Diese grösste Zahl erreicht man von der Null aus durch einen Schritt gegen
den Uhrzeigersinn, also durch Subtraktion von 1. Sie spielt also die Rolle
von $0 - 1 = -1$.

Der zweite Übergang kann weitgehend willkürlich gelegt werden. Man legt ihn so,
dass man binäre Zahlen, deren höchstwertiges Bit gleich 1 ist, als negativ
betrachtet und Zahlen, bei denen dieses Bit 0 ist, als positiv.

So lässt sich der Zahlenraum von $-8$ bis $+7$ mit 4-Bit-Worten darstellen.
Man nennt die hier gewählte Darstellung der negativen Zahlen die
Zweier-Komplement-Darstellung.

Was uns noch fehlt, ist ein Algorithmus, mit dem wir aus dem Bitmuster für eine
Zahl $a > 0 $ das Bitmuster der Zahl ($-a$) erhalten können. Schauen wir uns
dazu mal ein Beispiel an:

Die Zahl $3$ hat das Bitmuster $0011$. Wenn wir nun dieses Muster bitweise
invertieren (also jede 0 durch eine 1 ersetzen und jede 1 durch eine 0),
dann erhalten wir das Muster $1100$. Ein Blick in die obige Tabelle zeigt,
dass wir dicht neben dem richtigen Ergebnis landen, nämlich bei ($-4$).
Wenn wir nun zu dieser Zahl noch eine $1$ addieren, dann erhalten wir das
korrekte Bitmuster $1101$ für ($-3$).

Wenn wir das bitweise Inverse einer Zahl $a$ mit $a_{bi}$ bezeichnen und das
Zweier-Komplement von $a$ mit $a*$, dann lässt sich die obige Regel als Formel
schreiben:

\[
    a* = a_{bi} + 1
\]

Falls du noch an weiteren Informationen zu diesem Theme interessiert bist,
dann findest du viel ergänzendes Material im Elektroniktutor\cite{elektroniktutor}
von Detlef Mietke.

% ===================

\clearpage
\subsection*{Übungen}

% https://de.serlo.org/informatik/184941/aufgaben-zu-zahlensystemen

\subsubsection*{Aufgaben zum Umrechnen Binärzahlen in Dezimalzahlen%
\footnote{\href
    {https://www.matheretter.de/wiki/binar-dezimal}
    {https://www.matheretter.de/wiki/binar-dezimal}}}

\begin{ExerciseList}

    \Exercise Rechne die Zahl $11111_2$ in eine Dezimalzahl um.
    \Answer
    Um den Dezimalwert der Zahl $11111_2$ zu ermitteln zähle als erstes die Stellen
    der Zahl, hier: 4 (Beachte beim Zählen mit 0 beginnen) und ermittle den Stellenwert.

    \[
        %! suppress = MathOperatorEscape
        \begin{array}{lcl}
            11111_2 & = & 1\cdot 2^0 + 1\cdot 2^1 + 1\cdot 2^2 + 1\cdot 2^3 + 1\cdot 2^4     \\
            & = & 1\cdot 1 + 1\cdot 2 + 1\cdot 4 + 1\cdot 8 + 1\cdot 16 \\
            & = & 31
        \end{array}
    \]

    \Exercise Rechne die Zahl $1010101_2$ in eine Dezimalzahl um.
    \Answer
    Um den Dezimalwert der Zahl $1010101_2$ zu ermitteln zähle als erstes die Stellen
    der Zahl, hier: 4 (Beachte beim Zählen mit 0 beginnen) und ermittle den Stellenwert.

    \[
        %! suppress = MathOperatorEscape
        \begin{array}{lcl}
            1010101_2 & = & 1\cdot 2^0 + 0\cdot 2^1 + 1\cdot 2^2 + 0\cdot 2^3 + 1\cdot 4^4 + 0\cdot 4^5 + 1\cdot 4^6   \\
            & = & 1\cdot 1 + 0\cdot 2 + 1\cdot 4 + 0\cdot 8 + 1\cdot 16 + 0\cdot 32 + 1\cdot 64 \\
            & = & 85
        \end{array}
    \]

    \Exercise Rechne die Zahl $1100011000_2$ in eine Dezimalzahl um.
\end{ExerciseList}

\subsubsection*{Aufgaben zum Umrechnen Oktalzahlen in Dezimalzahlen%
\footnote{\href
    {https://www.matheretter.de/wiki/oktalzahlen}
    {https://www.matheretter.de/wiki/oktalzahlen}}}

\begin{ExerciseList}

    \Exercise Rechne die Zahl $173_8$ in eine Dezimalzahl um.
    \Answer
    Um den Dezimalwert der Zahl $173_8$ zu ermitteln zähle als erstes die Stellen
    der Zahl. Du erhältst eine Stellennummer: 2

    \[
        %! suppress = MathOperatorEscape
        \begin{array}{lcl}
            173_8 & = & 3\cdot 8^0 + 7\cdot 8^1 + 1\cdot 8^2    \\
            & = & 3\cdot 1 + 7\cdot 8 + 1\cdot 64 \\
            & = & 123
        \end{array}
    \]

    \Exercise Rechne die Zahl $2746_8$ in eine Dezimalzahl um.
    \Exercise Rechne die Zahl $11107_8$ in eine Dezimalzahl um.
\end{ExerciseList}

\subsubsection*{Aufgaben zum Umrechnen Hexadezimalzahlen in Dezimalzahlen%
\footnote{\href
    {https://www.matheretter.de/wiki/hexadezimal-dezimal}
    {https://www.matheretter.de/wiki/hexadezimal-dezimal}}}


\begin{ExerciseList}
    \Exercise Rechne die Zahl $123_{16}$ in eine Dezimalzahl um.
    \Answer
    Um den Dezimalwert der Zahl $123_{16}$ zu ermitteln zähle als erstes die Stellen
    der Zahl. Du erhältst eine Stellennummer: 2

    \[
        %! suppress = MathOperatorEscape
        \begin{array}{lcl}
            123_{16} & = & 3\cdot 16^0 + 2\cdot 16^1 + 1\cdot 16^2    \\
            & = & 3\cdot 1 + 2\cdot 16 + 1\cdot 256 \\
            & = & 291
        \end{array}
    \]

    \Exercise Rechne die Zahl $AAB_{16}$ in eine Dezimalzahl um.
    \Exercise Rechne die Zahl $FF_{16}$ in eine Dezimalzahl um.
\end{ExerciseList}

\subsubsection*{Aufgaben zum Umrechnen von Dezimalzahlen in Binärzahlen}
\begin{ExerciseList}
    \Exercise Rechne die Zahl $13_{10}$ in eine Binärzahl um.
    \Answer
    Um die Binärzahl der Zahl $13_{10}$ zu errechnen, ermittle als erstes den
    grössten Stellenwert zur Basis 2 mit dem man durch weitere Addition
    die Zahl beschreiben kann. Hier 8, da 16 der nächstgrössere Wert zu gross
    für diese Zahl wäre. Somit erhältst Du eine Stellennummer von 3.

    \[
        1\cdot 2^3 = 8
    \]

    Der Stellenwert der nächsten Stellennummer (2) ist gleich 4.

    \[
        1\cdot 2^2 = 4
    \]

    $8 + 4 = 12$, brauchen wir also noch eine 1 um $13_{10}$ darzustellen.
    Der Stellenwert der Stelle 0 ist 1, denn

    \[
        1\cdot 2^0 = 1
    \]

    Wenn wir jetzt die ermittelten Ziffern in eine Zeile schreiben
    (beachte Stelle 1 haben wir nicht gebraucht) folgt:

    \[
        13_{10} = 1101_2
    \]

    Und das ist auch schon die Lösung.

    \Exercise Rechne die Zahl $127_{10}$ in eine Binärzahl um.

    \Exercise Rechne die Zahl $2021_{10}$ in eine Binärzahl um.
    \Answer
    Um die Binärzahl der Zahl $2021_{10}$ zu errechnen benutzen wir die Methode
    der ganzzahligen Division. Dabei wird die Dezimalzahl ganzzahlig mit Rest
    durch die Basis 2 dividiert. Das Ergebnis dieser Division wird dann ebenfalls
    ganzzahlig mit Rest durch 2 dividiert. Dies geschieht so lange, bis das
    Ergebnis der Division 0 ist.

    \vskip1em
    Aus den Restwerten der ganzzahligen Divisionen ergibt sich am Ende die Binärzahl.

    \[
        %! suppress = MathOperatorEscape
        \begin{array}{rccrcc}
            2021 & : & 2 = & 1010 & Rest & 1 \\
            1010 & : & 2 = &  505 & Rest & 0 \\
            505 & : & 2 = &  252 & Rest & 1 \\
            252 & : & 2 = &  126 & Rest & 0 \\
            126 & : & 2 = &  63 & Rest & 0 \\
            63 & : & 2 = &  31 & Rest & 1 \\
            31 & : & 2 = &  15 & Rest & 1 \\
            15 & : & 2 = &  7 & Rest & 1 \\
            7 & : & 2 = &  3 & Rest & 1 \\
            3 & : & 2 = &  1 & Rest & 1 \\
            1 & : & 2 = &  0 & Rest & 1 \\
        \end{array}
    \]

    Die Binärzahl ergibt sich somit indem man die Restwerte von unten nach oben liest und aufschreibt.

    \[ 11111100101_2 \]

    Es ist weit verbreitet, Binärzahlen in Clustern zu schreiben, nicht zuletzt um die Umrechnung
    in andere Stellenwertsysteme sowie die bessere Lesbarkeit zu vereinfachen.
    \[ 0111\: 1110\: 0101_2 \]
\end{ExerciseList}

