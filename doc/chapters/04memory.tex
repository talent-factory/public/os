\section[Speicherverwaltung]{Speicherverwaltung\footnote{\href
    {https://de.wikipedia.org/wiki/Speicherverwaltung}
    {https://de.wikipedia.org/wiki/Speicherverwaltung}}}
\label{sec:speicherverwaltung}

Der Hauptspeicher (RAM) ist eine wichtige Ressource, die sehr sorgfältig
verwaltet werden muss. Was sich jeder Programmierer wünscht, ist ein privater,
unendlich grosser, unendlich schneller Speicher, der zudem nichtflüchtig ist,
d. h. seinen Inhalt nicht verliert, wenn der Strom abgeschaltet wird. Bestimmt
spielt auch der Kostenfaktor eine wichtige Rolle.


\subsection{Keine Speicherabstraktion}

Die einfachste Speicherabstraktion besteht darin, dass es überhaupt keine Abstraktion
gibt. Frühe Grossrechner (vor 1960), frühe Minicomputer (vor 1970) und frühe
Personalcomputer (vor 1980) hatten keine Speicherabstraktion. Jedes Programm
sah einfach den physischen Speicher. Wenn ein Programm eine Anweisung ausführte wie

\begin{minted}{sh}
    MOV REGISTER1, 1000
\end{minted}

hat der Computer einfach den Inhalt des physischen Speicherplatzes 1000 nach
REGISTER1 verschoben. Das Modell des Speichers, das dem Programmierer präsentiert
wurde, war also einfach ein physischer Speicher, eine Reihe von Adressen
von 0 bis zu einem gewissen Maximum, wobei jede Adresse einer Zelle entspricht,
die eine bestimmte Anzahl von Bits enthält, in der Regel acht.

Unter diesen Bedingungen war es nicht möglich, zwei Programme gleichzeitig
im Speicher laufen zu lassen. Wenn das erste Programm einen neuen Wert z.B.\
an die Stelle 2000 schrieb, würde dies den Wert löschen, den das zweite
Programm dort gespeichert hatte. Nichts würde mehr funktionieren und beide
Programme würden sofort abstürzen.

Wenn das System auf diese Weise organisiert ist, kann in der Regel nur ein
Prozess zur gleichen Zeit laufen. Sobald der Benutzer einen Befehl eingibt,
kopiert das Betriebssystem das angeforderte Programm von der Festplatte in
den Speicher und führt es aus. Wenn der Prozess beendet ist, zeigt das
Betriebssystem ein Eingabeaufforderungszeichen an und wartet auf einen neuen
Befehl des Benutzers. Wenn das Betriebssystem den Befehl erhält, lädt es
ein neues Programm in den Speicher und überschreibt das erste.

\image{03-01}{Einfache Speicherverwaltung}

Eine Möglichkeit, in einem System ohne Speicherabstraktion eine gewisse
Parallelität zu erreichen, besteht darin, mit mehreren Threads zu
programmieren. Da alle Threads in einem Prozess das gleiche Speicherabbild
sehen sollen, ist die Tatsache, dass sie dazu gezwungen sind, kein Problem.
Diese Idee funktioniert zwar, ist aber nur von begrenztem Nutzen, da man
oft möchte, dass nicht verwandte Programme gleichzeitig laufen, was die
Threads-Abstraktion nicht bietet.

\subsubsection{Mehrere Programme ohne Speicherabstraktion ausführen}

Aber auch ohne Speicherabstraktion ist es möglich, mehrere Programme
gleichzeitig laufen zu lassen. Das Betriebssystem muss lediglich den
gesamten Speicherinhalt in einer Datei auf der Festplatte speichern
(\emph{swapping}) und dann das nächste Programm aufrufen und ausführen.
Solange sich jeweils nur ein Programm im Speicher befindet, gibt es
keine Konflikte.

Eine andere Möglichkeit besteht darin, die einzelnen Programme an verschiedenen
Adressen im Speicher abzulegen.  Die frühen Modelle der IBM 360 lösten das Problem
wie folgt: Der Speicher wurde in 2-KB-Blöcke unterteilt, denen jeweils ein
4-Bit-Schutzschlüssel zugewiesen wurde, der in speziellen Registern innerhalb
der CPU gespeichert wurde.

Das erste Programm beginnt mit einem Sprung zur Adresse 24, die einen
MOV-Befehl enthält. Das zweite Programm beginnt mit einem Sprung zur
Adresse 28, die eine CMP-Anweisung enthält. Die Anweisungen, die für
diese Diskussion nicht relevant sind, werden nicht gezeigt.
Wenn die beiden Programme nacheinander in den Speicher geladen werden,
beginnend bei Adresse 0, ergibt sich die Situation von Abbildung~\ref{fig:03-02}.

\image{03-02}{Veranschaulichung des Verlagerungsproblems}

Nachdem die Programme geladen sind, können sie ausgeführt werden.
Da sie unterschiedliche Speicherschlüssel haben, kann keines das andere
beschädigen. Das Problem ist jedoch anderer Natur. Wenn das erste Programm
startet, führt es die Anweisung JMP 24 aus, die erwartungsgemäss zu der
Anweisung springt. Dieses Programm funktioniert normal.

Nachdem das erste Programm jedoch lange genug gelaufen ist, kann das
Betriebssystem beschliessen, das zweite Programm, das über das erste
geladen wurde, an der Adresse 16'384 auszuführen. Die erste ausgeführte
Anweisung ist JMP 28, die zur ADD-Anweisung im ersten Programm springt,
anstatt zur CMP-Anweisung, zu der sie eigentlich springen sollte.


\subsection{Eigene Adressräume}

Das Konzept eines Adressraums ist sehr allgemein und kommt in vielen
Zusammenhängen vor (z. B. Telefonnummern).

Adressräume müssen nicht zwangsläufig numerisch sein. Die Menge der
.com{-}Internet{-}domänen ist ebenfalls ein Adressraum. Dieser Adressraum
besteht aus allen Zeichenketten mit einer Länge von 2 bis 63 Zeichen,
die aus Buchstaben, Zahlen und Bindestrichen gebildet werden können,
gefolgt von .com.

Etwas schwieriger ist es, jedem Programm seinen eigenen Adressraum zu geben,
so dass die Adresse 28 in einem Programm einen anderen physikalischen
Ort bedeutet als die Adresse 28 in einem anderen Programm.


\subsubsection{Basis- und Grenzwertregister}

Diese einfache Lösung verwendet eine besonders einfache Version der
dynamischen Verlagerung. Dabei wird der Adressraum jedes Prozesses auf
einfache Weise einem anderen Teil des physischen Speichers zugeordnet.

Jedes Mal, wenn ein Prozess auf den Speicher zugreift, um einen Befehl
zu holen oder ein Datenwort zu lesen oder zu schreiben, addiert die
CPU-Hardware automatisch den Basiswert zu der vom Prozess erzeugten
Adresse, bevor sie die Adresse auf den Speicherbus schickt.
Gleichzeitig prüft sie, ob die angebotene Adresse gleich oder grösser
als der Wert im Grenzwertregister ist; in diesem Fall wird ein Fehler
erzeugt und der Zugriff abgebrochen.

\image{03-03}{Basis- und Grenzwertregister}

Die Verwendung von Basis- und Grenzwertregistern (Abbildung~\ref{fig:03-03})
ist eine einfache Möglichkeit, jedem Prozess einen eigenen Adressraum zuzuweisen,
da zu jeder erzeugten Speicheradresse automatisch der Inhalt des Basisregisters
hinzugefügt wird, bevor sie an den Speicher gesendet wird. In vielen Implementierungen
sind die Basis- und Grenzwertregister so geschützt, dass nur das
Betriebssystem sie ändern kann.


\subsubsection{Swapping}

Wenn der physische Speicher des Rechners gross genug ist, um alle Prozesse
aufzunehmen, sind die bisher beschriebenen Schemata mehr oder weniger ausreichend.
In der Praxis ist jedoch die Gesamtmenge an RAM, die von allen Prozessen
benötigt wird, oft viel grösser als der verfügbare Speicherplatz.

Auf einem typischen System werden etwa 50--100 Prozesse gestartet, sobald der
Computer hochgefahren ist. Ein solcher Prozess kann leicht 5--10 MB Speicherplatz
belegen. Andere Hintergrundprozesse überprüfen den Posteingang, eingehende
Netzwerkverbindungen und viele andere Dinge.

Anwendungsprogramme wie Photoshop können heutzutage leicht 500 MB allein zum
Booten benötigen und viele Gigabyte, sobald sie mit der Datenverarbeitung beginnen.
Alle Prozesse immer im Speicher zu halten, erfordert daher eine enorme Menge an
Speicherplatz und kann nicht durchgeführt werden, wenn nicht genügend Speicher
vorhanden ist.

Im Laufe der Jahre wurden zwei allgemeine Ansätze zur Bewältigung von
Speicherüberlastungen entwickelt. Die einfachste Strategie, das so genannte
\emph{Swapping}, besteht darin, jeden Prozess in seiner Gesamtheit einzubringen,
ihn eine Weile laufen zu lassen und dann wieder auf die Festplatte zu legen.

Die Funktionsweise eines Tauschsystems ist in Abbildung~\ref{fig:03-04} dargestellt.
Zu Beginn befindet sich nur Prozess A im Speicher. Dann werden die Prozesse B und C erstellt
oder von der Festplatte eingelagert. A wird auf die Festplatte ausgelagert.
Dann kommt D rein und B geht raus.

Wenn durch Auslagerung mehrere Löcher im Speicher entstehen, ist es möglich,
sie alle zu einem grossen zusammenzufassen, indem alle Prozesse so weit wie
möglich nach unten verschoben werden. Diese Technik wird als
Speicherverdichtung bezeichnet.

\image{03-04}{Speicherzuweisung auf der Zeitachse}

Die andere Strategie, der so genannte virtuelle Speicher, ermöglicht die
Ausführung von Programmen, auch wenn sie sich nur teilweise im Hauptspeicher befinden.

Wenn Speicher dynamisch zugewiesen wird, muss das Betriebssystem ihn verwalten.
Im Allgemeinen gibt es zwei Möglichkeiten, die Speichernutzung zu überwachen:
Bitmaps und Listenverwaltung.

\subsubsection{Speicherverwaltung mit Bitmaps}

Mit einer Bitmap wird der Speicher in Zuweisungseinheiten unterteilt, die so
klein wie einige Wörter und so gross wie mehrere Kilobytes sind. Jeder
Zuordnungseinheit entspricht einem Bit in der Bitmap($0\rightarrow$ frei;
$1\rightarrow$ belegt).
Abbildung~\ref{fig:03-06} zeigt einen Teil des Speichers und die entsprechende Bitmap.

\image{03-06}{Bitmaps (a) und verkettete Listen (b)}

Die Grösse der Zuweisungseinheit ist ein wichtiges Designelement. Je kleiner
die Zuweisungseinheit, desto grösser die Bitmap. Aber selbst bei einer
Zuordnungseinheit von nur 4 Byte wird für 32 Bits Speicher nur 1 Bit der
Map benötigt. Bei einem Speicher von $32 n$ Bits werden $n$ Map-Bits benötigt,
so dass die Bitmap nur $\frac{1}{32}$ des Speichers einnimmt.
Wenn die Speicherplatzeinheit gross gewählt wird, ist die Bitmap kleiner.

Eine Bitmap bietet eine einfache Möglichkeit, den Überblick über die
Speicherwörter in einem festen Speicherbereich zu behalten, da die Grösse
der Bitmap nur von der Grösse des Speichers und der Grösse der
Zuweisungseinheit abhängt. Das Hauptproblem besteht darin, dass der
Speichermanager einen Prozess mit $k$ Einheiten in den Speicher zu bringen,
die Bitmap durchsuchen muss, um $k$ aufeinanderfolgenden 0-Bits in der
Map zu finden. Das Durchsuchen einer Bitmap bestimmter Länge ist ein
langsamer Vorgang. Dies ist ein Argument gegen Bitmaps.

\subsubsection{Speicherverwaltung mit verketteten Listen}

Eine andere Möglichkeit, den Speicher im Auge zu behalten, besteht darin,
eine verkettete Liste von zugewiesenen und freien Speichersegmenten
zu führen, wobei ein Segment entweder einen Prozess enthält oder
eine leere Lücke zwischen zwei Prozessen darstellt.

\image{03-07}{Vier Nachbarkombinationen für den abschliesenden Prozess X}

Wenn die Prozesse und Löcher in einer nach Adressen sortierten Liste geführt werden,
können verschiedene Algorithmen verwendet werden, um einem neuen Prozess
(oder einem bestehenden Prozess, der von der Festplatte eingelagert wird)
Speicher zuzuweisen. Wir gehen davon aus, dass der Speichermanager weiss,
wie viel Speicher zuzuweisen ist.

Der einfachste Algorithmus ist first fit. Der Speicherverwalter durchsucht
die Liste der Segmente, bis er ein Loch findet, das gross genug ist. Die Lücke
wird dann in zwei Teile zerlegt, einen für den Prozess und einen für den
ungenutzten Speicher, ausser in dem statistisch unwahrscheinlichen Fall einer
exakten Anpassung. First fit ist ein schneller Algorithmus, weil er so wenig
wie möglich sucht.

Eine geringfügige Abwandlung von first fit ist next fit. Es funktioniert
auf die gleiche Weise wie first fit, mit dem Unterschied, dass es sich merkt,
wo es sich befindet, wenn es ein passendes Loch findet. Wenn es das nächste Mal
aufgerufen wird, um ein Loch zu finden, beginnt es mit der Suche in der Liste
an der Stelle, an der es beim letzten Mal aufgehört hat, anstatt immer am Anfang,
wie es bei first fit der Fall ist.

Ein weiterer bekannter und weit verbreiteter Algorithmus ist best fit.
Best fit durchsucht die gesamte Liste vom Anfang bis zum Ende und nimmt das
kleinste Loch, das geeignet ist. Anstatt ein grosses Loch zu zerlegen,
welches möglicherweise später benötigt wird, versucht best fit, eine Loch zu finden,
das der tatsächlich benötigten Grösse nahe kommt.

Best fit ist langsamer als first fit, weil es bei jedem Aufruf die gesamte Liste
durchsuchen muss. Überraschenderweise wird dabei auch mehr Speicherplatz verschwendet
als bei first fit oder next fit, da der Speicher mit winzigen, nutzlosen
Löchern gefüllt wird. First fit erzeugt im Durchschnitt grössere Löcher.

Ein weiterer Zuweisungsalgorithmus ist quick fit, der separate Listen für
einige der am häufigsten angeforderten Grössen unterhält. Beispielsweise könnte
er eine Tabelle mit $n$ Einträgen haben, in der der erste Eintrag ein Zeiger
auf den Kopf einer Liste von 4-KB-Löchern ist, der zweite Eintrag ein Zeiger
auf eine Liste von 8-KB-Löchern, der dritte Eintrag ein Zeiger auf 12-KB-Löcher
und so weiter.

\subsection{Virtueller Speicher}

Während Basis- und Grenzregister zur Abstraktion von Adressräumen
verwendet werden können, gibt es ein weiteres Problem, das gelöst werden muss:
die Verwaltung von \emph{Bloatware}\footnote{%
\href{https://de.wikipedia.org/wiki/Bloatware}{https://de.wikipedia.org/wiki/Bloatware}}.
Während die Grösse des Speichers schnell zunimmt, wächst die Grösse der Software
viel schneller. In den 1980er Jahren betrieben viele Universitäten ein
Timesharing-System mit Dutzenden von Benutzern, die gleichzeitig auf einer
4-MB-VAX arbeiteten. Heute empfiehlt Microsoft für das 64-Bit-Windows 8 mindestens 2 GB\@.
Der Trend zu Multimedia stellt noch höhere Anforderungen an den Speicher.

Als Folge dieser Entwicklungen besteht die Notwendigkeit, Programme auszuführen,
die zu gross sind, um in den Speicher zu passen, und es besteht sicherlich die
Notwendigkeit, Systeme zu haben, die mehrere gleichzeitig laufende Programme
unterstützen, von denen jedes in den Speicher passt, die aber alle zusammen den
Speicher übersteigen. Das Auslagern ist keine attraktive Option, da eine typische
SATA-Festplatte eine Spitzenübertragungsrate von mehreren hundert MB/s hat,
was bedeutet, dass es Sekunden dauert, ein 1-GB-Programm auszulagern, und ebenso lange,
ein 1-GB-Programm einzulagern.

Die Methode\cite{fotheringh61}, die entwickelt wurde, ist als virtueller Speicher bekannt
geworden. Der Grundgedanke hinter dem virtuellen Speicher ist, dass jedes
Programm seinen eigenen Adressraum hat, der in Abschnitte, so genannte
Seiten (\emph{pages}), unterteilt ist. Jede Seite ist ein zusammenhängender
Bereich von Adressen. Diese Seiten werden auf den physischen Speicher
abgebildet, aber nicht alle Seiten müssen sich gleichzeitig im physischen
Speicher befinden, um das Programm auszuführen.

Wenn das Programm auf einen Teil seines Adressraums verweist, der sich im
physischen Speicher befindet, führt die Hardware die erforderliche Zuordnung
im laufenden Betrieb durch. Wenn das Programm auf einen Teil seines Adressraums
verweist, der sich nicht im physischen Speicher befindet, wird das
Betriebssystem alarmiert, um den fehlenden Teil zu holen und den
fehlgeschlagenen Befehl erneut auszuführen.


\subsubsection{Paging}

Die meisten virtuellen Speichersysteme verwenden eine Technik namens Paging.
Auf jedem Computer verweisen Programme auf eine Reihe von Speicheradressen.
Wenn ein Programm eine Anweisung ausführt wie

\begin{minted}{sh}
    MOV REG, 1000
\end{minted}

kopiert er den Inhalt der Speicheradresse 1'000 nach REG. Adressen können durch
Indizierung, Basisregister, Segmentregister und auf andere Weise erzeugt werden.

\image{03-08}{Position und Funktion der MMU}

Diese vom Programm erzeugten Adressen werden als virtuelle Adressen bezeichnet
und bilden den virtuellen Adressraum. Bei Computern ohne virtuellen Speicher wird
die virtuelle Adresse direkt auf den Speicherbus gelegt und bewirkt, dass das
physische Speicherwort mit derselben Adresse gelesen oder geschrieben wird.
Bei der Verwendung von virtuellem Speicher gehen die virtuellen Adressen nicht
direkt an den Speicherbus. Stattdessen gehen sie zu einer MMU
(\emph{Memory Management Unit}\footnote{%
\href
    {https://en.wikipedia.org/wiki/Memory\_management\_unit}
    {https://en.wikipedia.org/wiki/Memory\_management\_unit}}),
die die virtuellen Adressen auf die physikalischen Speicheradressen abbildet, 
wie in Abbildung~\ref{fig:03-08} dargestellt.


Ein sehr einfaches Beispiel dafür, wie diese Zuordnung funktioniert,
ist in Abbildung~\ref{fig:03-09} dargestellt. In diesem Beispiel haben wir
einen Computer, der 16-Bit-Adressen von 0 bis 64K - 1 erzeugt.
Dies sind die virtuellen Adressen. Dieser Computer verfügt jedoch nur über 32 KB
physischen Speicher. Obwohl also 64-KB-Programme geschrieben werden können,
können sie nicht vollständig in den Speicher geladen und ausgeführt werden.
Eine vollständige Kopie des Kernabbilds eines Programms (bis zu 64 KB) muss
jedoch auf der Festplatte vorhanden sein, so dass bei Bedarf Teile davon
eingebracht werden können.

\image{03-09}{Beziehung zwischen virtuellen und physischen Speicheradressen}

Der virtuelle Adressraum besteht aus Einheiten fester Grösse, die
Seiten (\emph{pages}) genannt werden. Die korrespondierenden Einheiten
im physischen Speicher werden \emph{page frames} genannt. Die \emph{pages} und
\emph{page frames} sind im Allgemeinen gleich gross. In diesem Beispiel sind
sie 4 KB gross, aber in realen Systemen werden Seitengrössen von 512 Byte
bis zu einem Gigabyte verwendet. Bei 64 KB virtuellem Adressraum und 32 KB
physischem Speicher ergeben sich 16 virtuelle \emph{pages} und
8 \emph{page frames}. Übertragungen zwischen RAM und Festplatte erfolgen
immer in ganzen Seiten. Viele Prozessoren unterstützen mehrere Seitengrössen,
die je nach Bedarf des Betriebssystems gemischt und angepasst werden können.


Die Notation in Abbildung~\ref{fig:03-09} ist wie folgt. Der mit 0K-4K
gekennzeichnete Bereich bedeutet, dass die virtuellen oder physischen
Adressen in dieser Seite 0 bis 4095 sind. Der Bereich 4K-8K bezieht sich
auf die Adressen 4096 bis 8191 und so weiter. Jede Seite enthält
genau 4096 Adressen, die mit einem Vielfachen von 4096 beginnen und
mit einem Bruchteil eines Vielfachen von 4096 enden.

Wenn das Programm versucht, auf die Adresse 0 zuzugreifen, z. B. mit der Anweisung

\begin{minted}{sh}
    MOV REG, 0
\end{minted}

dann wird die virtuelle Adresse 0 an die MMU gesendet. Die MMU sieht, dass diese
virtuelle Adresse in die Seite 0 (0 bis 4095) fällt, die gemäss ihrer Abbildung
\emph{page frame} 2 (8192 bis 12287) entspricht. Sie wandelt also die Adresse
in 8192 um und gibt die Adresse 8192 auf dem Bus aus. Der Speicher weiss nichts
von der MMU und sieht nur eine Anfrage zum Lesen oder Schreiben der Adresse 8192,
der er nachkommt. Die MMU hat also alle virtuellen Adressen zwischen 0 und 4095
auf die physikalischen Adressen 8192 bis 12287 abgebildet.

Ähnlich verhält es sich mit der Anweisung

\begin{minted}{sh}
    MOV REG, 8192
\end{minted}

welche umgewandlt wird in

\begin{minted}{sh}
    MOV REG, 24576
\end{minted}

weil die virtuelle Adresse 8192 (in der virtuellen Seite 2) auf 24576 (im physischen
\emph{page frame} 6) abgebildet wird. Ein drittes Beispiel: Die virtuelle Adresse 20'500
liegt 20 Byte vom Anfang der virtuellen Seite 5 (virtuelle Adressen 20480 bis 24575)
entfernt und wird auf die physische Adresse $12288 + 20 = 12308$ abgebildet.

Die Möglichkeit, die 16 virtuellen Seiten auf jeden der acht \emph{page frame} abzubilden,
indem die MMU-Map entsprechend eingestellt wird, löst nicht das Problem, dass der
virtuelle Adressraum grösser ist als der physische Speicher.
Da wir nur acht physische \emph{page frame} haben, werden nur acht der virtuellen
Seiten in Abbildung~\ref{fig:03-09} auf den physischen Speicher abgebildet.
Die anderen, die in der Abbildung durch ein Kreuz dargestellt sind, werden nicht
abgebildet. In der tatsächlichen Hardware hält ein Present/absent-Bit fest,
welche Seiten physisch im Speicher vorhanden sind.

Wenn die MMU feststellt, dass eine angesprochene Seite nicht existiert, dann wird
ein page fault Fehler ausgelöst. Das Betriebssystem wählt ein wenig benutztes
\emph{page frame} aus und schreibt ihren Inhalt zurück auf die Festplatte.
Dann holt es (ebenfalls von der Festplatte) die Seite, auf die in der soeben
freigegebenen \emph{page frame} verwiesen wurde, ändert die Zuordnung und
startet die Anweisung erneut.

\subsubsection{Page Tables}

In einer einfachen Implementierung lässt sich die Zuordnung von virtuellen Adressen
zu physischen Adressen wie folgt zusammenfassen: Die virtuelle Adresse wird in
eine virtuelle Seitennummer (höherwertige Bits) und einen Offset (niederwertige Bits)
aufgeteilt. Bei einer 16-Bit-Adresse und einer Seitengröße von 4 KB könnten zum
Beispiel die oberen 4 Bits eine der 16 virtuellen Seiten angeben, und die
unteren 12 Bits würden dann den Byte-Offset (0 bis 4095) innerhalb der ausgewählten
Seite angeben. Eine Aufteilung mit 3 oder 5 oder einer anderen Anzahl von Bits
für die Seite ist jedoch auch möglich. Unterschiedliche Aufteilungen bedeuten
unterschiedliche Seitengrössen.

\image{03-10}{Arbeitsweise der MMU mit 16 4-KB-Seiten}

Die Nummer der virtuellen Seite wird als Index in der Seitentabelle verwendet,
um den Eintrag für diese virtuelle Seite zu finden. Anhand des Eintrags in der
Seitentabelle wird die \emph{page frame} (falls vorhanden) ermittelt. Diese wird
an das höherwertige Ende des Off-Sets angehängt und ersetzt die virtuelle Seitennummer,
um eine physische Adresse zu bilden, die an den Speicher gesendet werden kann.

Der Zweck der Seitentabelle besteht also darin, virtuelle Seiten auf \emph{page frames}
abzubilden. Mathematisch gesehen ist die Seitentabelle eine Funktion mit der
virtuellen Seitennummer als Argument und der physischen \emph{page frames}  als Ergebnis.
Die Verwendung des Ergebnisses dieser Funktion kann das virtuelle Seitenfeld in einer
virtuellen Adresse durch ein \emph{page frame} ersetzt werden, wodurch eine
physische Speicheradresse entsteht.


\input{chapters/04memory-exercises}
