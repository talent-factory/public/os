\section{Prozesse und Threads}
\label{sec:prozesse}

Wir werden uns nun eingehend damit befassen, wie Betriebssysteme im allgemeinen und MINIX 3
im besonderen konzipiert und aufgebaut sind. Das zentrale Konzept eines jeden Betriebssystems
ist der Prozess: eine Abstraktion eines laufenden Programms. Alles andere hängt von diesem
Konzept ab, und es ist wichtig, dass der Betriebssystementwickler und der Student
dieses Konzept gut versteht.

\subsection{Einführung in die Prozesse}

Alle modernen Computer können mehrere Dinge gleichzeitig tun. Während ein Benutzerprogramm
läuft, kann ein Computer auch von einer Festplatte lesen und Text auf einem Bildschirm oder
Drucker ausgeben. In einem Multiprogramming-System schaltet die CPU auch von einem Programm
zum anderen um, wobei jedes Programm einige Dutzend oder Hunderte von Millisekunden
lang ausgeführt wird. Während die CPU streng genommen zu jedem Zeitpunkt nur ein Programm
ausführt, kann sie im Laufe einer Sekunde an mehreren Programmen arbeiten, was dem Benutzer
die Illusion der Parallelität vermittelt. Manchmal spricht man in diesem Zusammenhang von
Pseudoparallelität, um sie von der echten Hardware-Parallelität von Multiprozessorsystemen
zu unterscheiden. Für den Menschen ist es schwer, den Überblick über mehrere parallele
Aktivitäten zu behalten. Daher haben die Entwickler von Betriebssystemen im Laufe der
Jahre ein konzeptionelles Modell (sequenzielle Prozesse) entwickelt, das den Umgang mit
Parallelität erleichtert. Dieses Modell, seine Verwendung und einige seiner Konsequenzen
sind Gegenstand dieses Kapitels.

\subsubsection{Das Prozessmodell}

In diesem Modell ist die gesamte lauffähige Software auf dem Computer, manchmal einschliesslich
des Betriebssystems, in einer Reihe von sequentiellen Prozessen oder kurz Prozessen organisiert.
Ein Prozess ist lediglich ein ausführendes Programm, einschliesslich der aktuellen Werte des
Programmzählers, der Register und der Variablen. Konzeptuell hat jeder Prozess seine eigene
virtuelle CPU. In Wirklichkeit schaltet die reale CPU natürlich von Prozess zu Prozess hin
und her, aber um das System zu verstehen, ist es viel einfacher, sich eine Sammlung von
Prozessen vorzustellen, die (pseudo-) parallel laufen, als zu versuchen, den Überblick darüber
zu behalten, wie die CPU von Programm zu Programm schaltet. Dieses schnelle Hin- und Herwechseln
wird Multiprogramming genannt.

\image{02-01}{Multiprogrammierung von vier Programmen/Prozessen}

In Abbildung~\ref{fig:02-01} sehen wir einen Computer, der vier Programme im Speicher
multiprogrammiert. Unter (b) sehen wir vier Prozesse, jeder mit seinem eigenen Kontrollfluss
(d.h.\ seinem eigenen Programmzähler), und jeder läuft unabhängig von den anderen. Natürlich
gibt es nur einen physischen Programmzähler. Wenn ein Prozess läuft, wird sein logischer
Programmzähler in den realen Programmzähler geladen. Wenn er vorläufig beendet ist, wird
der physische Programmzähler im logischen Programmzähler des Prozesses im Speicher gespeichert.
Unter (c) sehen wir, dass über ein ausreichend langes Zeitintervall betrachtet, alle Prozesse
Fortschritte gemacht haben, aber zu jedem beliebigen Zeitpunkt nur ein Prozess tatsächlich läuft.

Da die CPU zwischen den Prozessen hin und her schaltet, ist die Geschwindigkeit, mit der ein
Prozess seine Berechnungen durchführt, nicht einheitlich und wahrscheinlich nicht einmal
reproduzierbar, wenn dieselben Prozesse erneut ausgeführt werden. Daher dürfen Prozesse nicht
mit eingebauten Annahmen über das \emph{Timing} programmiert werden.

\subsubsection{Prozess-Erstellung}

Betriebssysteme müssen auf irgendeine Weise sicherstellen, dass alle erforderlichen Prozesse
vorhanden sind. In sehr einfachen Systemen oder in Systemen, die nur für die Ausführung einer
einzigen Anwendung (z. B. die Steuerung eines Geräts in Echtzeit) konzipiert sind, kann es
möglich sein, dass alle jemals benötigten Prozesse beim Hochfahren des Systems vorhanden sind.
In Mehrzwecksystemen ist jedoch eine Möglichkeit erforderlich, um Prozesse während des Betriebs
nach Bedarf zu erstellen und zu beenden. Wir werden uns nun einige dieser Probleme ansehen.

Es gibt vier wesentliche Ereignisse, die zur Entstehung von Prozessen führen:

\begin{enumerate}
    \item Initialisierung des Systems
    \item Ausführung eines Systemaufrufs zur Prozesserzeugung durch einen laufenden Prozess
    \item Ein Benutzerantrag zur Erstellung eines neuen Prozesses
    \item Initiierung eines Batch-Jobs
\end{enumerate}

Wenn ein Betriebssystem hochgefahren wird, werden oft mehrere Prozesse erstellt. Einige
davon sind Vordergrundprozesse, d. h. Prozesse, die mit (menschlichen) Benutzern interagieren
und für diese Arbeit verrichten. Andere sind Hintergrundprozesse, die nicht mit bestimmten
Benutzern verbunden sind, sondern eine bestimmte Funktion haben. Ein Hintergrundprozess
kann z. B. so konzipiert sein, dass er eingehende Anfragen für Webseiten, die auf diesem
Rechner gehostet werden, annimmt und beim Eintreffen einer Anfrage aufwacht, um die Anfrage
zu bearbeiten. Prozesse, die im Hintergrund bleiben, um Aktivitäten wie Webseiten, Druckvorgänge
usw. zu bearbeiten, werden \emph{Daemons} genannt. Grosse Systeme haben in der Regel Dutzende
von ihnen. In MINIX 3 kann das Programm \cmd{ps} verwendet werden, um die laufenden Prozesse
aufzulisten.

Zusätzlich zu den Prozessen, die beim Booten erstellt werden, können auch danach neue Prozesse
erstellt werden. Oftmals gibt ein laufender Prozess Systemaufrufe aus, um einen oder mehrere
neue Prozesse zu erstellen, die ihn bei der Erledigung seiner Aufgaben unterstützen. Die
Erstellung neuer Prozesse ist besonders nützlich, wenn sich die zu erledigende Arbeit leicht
in Form mehrerer verwandter, aber ansonsten unabhängig voneinander interagierender Prozesse
formulieren lässt. Bei der Kompilierung eines grossen Programms ruft das make-Programm
beispielsweise den C-Compiler auf, um die Quelldateien in Objektcode umzuwandeln, und ruft
dann das install-Programm auf, um das Programm an seinen Bestimmungsort zu kopieren,
Eigentumsrechte und Berechtigungen festzulegen usw. In MINIX 3 besteht der C-Compiler s
elbst eigentlich aus mehreren verschiedenen Programmen, die zusammenarbeiten. Dazu gehören
ein Präprozessor, ein C-Sprachparser, ein Assembler-Code-Generator, ein Assembler und ein Linker.

In interaktiven Systemen kann der Benutzer ein Programm durch Eingabe eines Befehls starten.
In MINIX 3 ermöglichen virtuelle Konsolen dem Benutzer, ein Programm zu starten, z.B.\ einen
Compiler, und dann auf eine andere Konsole zu wechseln und ein anderes Programm zu starten,
z.B.\ um die Dokumentation zu bearbeiten, während der Compiler läuft.

Die letzte Situation, in der Prozesse erstellt werden, trifft nur auf die Batch-Systeme zu, die
auf grossen Mainframes zu finden sind. Hier können Benutzer Batch-Aufträge an das System
übermitteln. Wenn das Betriebssystem entscheidet, dass es über die Ressourcen für einen
weiteren Auftrag verfügt, erstellt es einen neuen Prozess und führt den nächsten Auftrag
aus der Eingabewarteschlange in diesem Prozess aus.

Technisch gesehen wird in all diesen Fällen ein neuer Prozess erstellt, indem ein bestehender
Prozess einen Systemaufruf zur Prozesserstellung ausführt. Dieser Prozess kann ein laufender
Benutzerprozess, ein von der Tastatur oder der Maus aufgerufener Systemprozess oder ein
Batchmanager-Prozess sein. Dieser Prozess führt einen Systemaufruf aus, um den neuen Prozess
zu erstellen. Dieser Systemaufruf weist das Betriebssystem an, einen neuen Prozess zu
erstellen und gibt direkt oder indirekt an, welches Programm in diesem Prozess ausgeführt
werden soll.

In MINIX 3 gibt es nur einen einzigen Systemaufruf, um einen neuen Prozess zu erzeugen:
\texttt{fork()}. Dieser Aufruf erzeugt einen exakten Klon des aufrufenden Prozesses.
Nach dem Fork haben die beiden Prozesse, der Eltern- und der Kindprozess, das gleiche
Speicherabbild, die gleichen Umgebungsstrings und die gleichen offenen Dateien. Das ist
alles, was vorhanden ist. Normalerweise führt der Kindprozess dann \texttt{execve()} oder
einen ähnlichen Systemaufruf aus, um sein Speicherabbild zu ändern und ein neues
Programm auszuführen. Wenn ein Benutzer beispielsweise einen Befehl, z. B. \cmd{sort}, in
die Shell eintippt, startet die Shell einen Kindprozess, und der Kindprozess führt \texttt{sort}
aus. Der Grund für diesen zweistufigen Prozess ist, dass der Kindprozess seine Dateideskriptoren
nach dem Fork, aber vor dem \texttt{execve()} manipulieren kann, um eine Umleitung der
Standardeingabe, der Standardausgabe und des Standardfehlers zu erreichen.

Sowohl in MINIX 3 als auch in UNIX haben nach der Erzeugung eines Prozesses sowohl der Eltern- als
auch der Kindprozess ihren eigenen Adressraum. Wenn einer der beiden Prozesse ein Wort in seinem
Adressraum ändert, ist diese Änderung für den anderen Prozess nicht sichtbar. Der anfängliche
Adressraum des Kindprozesses ist eine Kopie des Adressraums des Elternprozesses, aber es gibt
zwei verschiedene Adressräume; kein beschreibbarer Speicher wird gemeinsam genutzt. Es ist
jedoch möglich, dass ein neu erstellter Prozess einige der anderen Ressourcen seines Erzeugers
mitbenutzt, z. B. offene Dateien.

\subsubsection{Prozess-Beendigung}

Nachdem ein Prozess erstellt worden ist, beginnt er zu laufen und erledigt seine Aufgabe. Nichts hält
jedoch ewig, nicht einmal Prozesse. Früher oder später wird der neue Prozess beendet, normalerweise
aufgrund einer der folgenden Bedingungen:

\begin{enumerate}
    \item Normaler Ausgang (freiwillig)
    \item Fehlerausgang (freiwillig)
    \item Fataler Fehler (unfreiwillig)
    \item Beendet durch einen anderen Prozess (unfreiwillig)
\end{enumerate}

Die meisten Prozesse werden beendet, weil sie ihre Arbeit getan haben. Wenn ein Compiler das ihm
übergebene Programm kompiliert hat, führt er einen Systemaufruf aus, um dem Betriebssystem mitzuteilen,
dass er fertig ist. Dieser Aufruf ist der \texttt{exit()} in MINIX 3. Bildschirmorientierte Programme
unterstützen auch das freiwillige Beenden. So gibt es bei Editoren immer eine Tastenkombination,
die der Benutzer aufrufen kann, um den Prozess anzuweisen, die Arbeitsdatei zu speichern, alle
geöffneten temporären Dateien zu löschen und sich zu beenden.

Der zweite Grund für den Abbruch ist, dass der Prozess einen schwerwiegenden Fehler feststellt.
Wenn ein Benutzer zum Beispiel den Befehl

\begin{minted}{C}
    cc file.c
\end{minted}

um das Programm \texttt{file.c} zu kompilieren, und keine solche Datei existiert, beendet
sich der Compiler einfach.

Der dritte Grund für den Abbruch ist ein Fehler, der durch den Prozess verursacht wurde, vielleicht
durch einen Programmfehler. Beispiele hierfür sind die Ausführung einer unzulässigen Anweisung,
der Verweis auf nicht existierenden Speicher oder die Division durch Null. In MINIX 3 kann ein
Prozess dem Betriebssystem mitteilen, dass er bestimmte Fehler selbst behandeln möchte.
In diesem Fall wird der Prozess signalisiert (unterbrochen), anstatt beendet zu werden,
wenn einer der Fehler auftritt.

Der vierte Grund für die Beendigung eines Prozesses ist, dass ein Prozess einen Systemaufruf ausführt,
der das Betriebssystem auffordert, einen anderen Prozess zu beenden. In MINIX 3 ist dieser Aufruf \texttt{kill}.
Natürlich muss der Killer die nötige Berechtigung haben, den Prozess zu töten. In manchen Systemen werden
bei der Beendigung eines Prozesses, sei es freiwillig oder aus anderen Gründen, alle von ihm erzeugten
Prozesse sofort mit beendet. MINIX 3 arbeitet jedoch nicht auf diese Weise.

\subsubsection{Prozess-Hierarchien}

Wenn in einigen Systemen ein Prozess einen weiteren Prozess erzeugt, bleiben Eltern- und Kindprozess auf
bestimmte Weise miteinander verbunden. Das Kind kann selbst weitere Prozesse erzeugen und so eine
Prozesshierarchie bilden. Im Gegensatz zu Pflanzen und Tieren hat ein
Prozess nur einen Elternteil (aber null, ein, zwei oder mehr Kinder).

In MINIX 3 können ein Prozess, seine Kinder und weitere Nachkommen zusammen eine Prozessgruppe bilden.
Wenn ein Benutzer ein Signal von der Tastatur aus sendet, kann das Signal an alle Mitglieder der Prozessgruppe,
die zur Zeit mit der Tastatur verbunden sind, übermittelt werden. Dies ist signalabhängig. Wenn ein Signal an
eine Gruppe gesendet wird, kann jeder Prozess das Signal auffangen, ignorieren oder die Standardaktion ausführen,
d. h. durch das Signal beendet werden.

Als einfaches Beispiel für die Verwendung von Prozessbäumen wollen wir uns ansehen, wie sich MINIX 3 initialisiert.
Zwei spezielle Prozesse, der Reinkarnationsserver und \texttt{init}, sind im Boot-Image vorhanden. Die Aufgabe
des Reinkarnationsservers ist es, Treiber und Server (neu) zu starten.

Im Gegensatz dazu führt \texttt{init} das Skript \texttt{/etc/rc} aus, das den Reinkarnationsserver dazu veranlasst,
Befehle zum Starten der nicht im Boot-Image enthaltenen Treiber und Server zu erteilen. Diese Prozedur macht
die so gestarteten Treiber und Server zu Kindern des Reinkarnationsservers, so dass der Reinkarnationsserver
informiert wird und sie neu starten (d. h. reinkarnieren) kann, falls einer von ihnen jemals ausfällt.
Dieser Mechanismus soll es MINIX 3 ermöglichen, einen Treiber- oder Serverabsturz zu tolerieren, da automatisch
ein neuer Treiber gestartet wird. In der Praxis ist das Ersetzen eines Treibers jedoch viel einfacher als das
Ersetzen eines Servers, da es weniger Rückwirkungen auf andere Bereiche des Systems hat.

Wenn \texttt{init} damit fertig ist, liest es die Konfigurationsdatei \texttt{/etc/ttytab}, um festzustellen,
welche Terminals und virtuellen Terminals existieren. Init leitet (\texttt{fork()})für jedes Terminal einen
\texttt{getty}-Prozess ein, zeigt eine Anmeldeaufforderung an und wartet dann auf eine Eingabe.
Wenn ein Name eingegeben wird, führt getty (\texttt{exec()}) einen Anmeldeprozess mit dem Namen als Argument
aus. Wenn sich der Benutzer erfolgreich anmeldet, führt login die Shell des Benutzers aus. Die Shell ist
also ein Kind von \texttt{init}. Benutzerbefehle erzeugen Kinder der Shell, die wiederum Enkelkinder
von \texttt{init} sind. Diese Abfolge von Ereignissen ist ein Beispiel dafür, wie Prozessbäume verwendet
werden.

\subsubsection{Prozesszustände}

Obwohl jeder Prozess eine unabhängige Einheit ist, mit eigenen Programmzählerregistern, Stack, offenen Dateien,
Alarmen und anderen internen Zuständen, müssen Prozesse oft mit anderen Prozessen interagieren, kommunizieren
und sich synchronisieren. Ein Prozess kann z. B. eine Ausgabe erzeugen, die ein anderer Prozess als Eingabe
verwendet. In diesem Fall müssen die Daten zwischen den Prozessen ausgetauscht werden. In dem Shell-Befehl

\begin{minted}{sh}
    cat chapter1 chapter2 chapter3 | grep tree
\end{minted}

der erste Prozess, der \cmd{cat} ausführt, verkettet drei Dateien und gibt sie aus. Der zweite Prozess,
der \cmd{grep} ausführt, wählt alle Zeilen aus, die das Wort `tree' enthalten. Abhängig von der relativen
Geschwindigkeit der beiden Prozesse, kann es vorkommen, dass \texttt{grep} bereit ist, zu laufen, aber
keine Eingabe auf ihn wartet. Er muss dann blockieren, bis eine Eingabe verfügbar ist.

Wenn ein Prozess blockiert wird, geschieht dies, weil er logischerweise nicht fortgesetzt werden kann,
in der Regel weil er auf eine Eingabe wartet, die noch nicht verfügbar ist. Es ist auch möglich, dass
ein Prozess, der konzeptionell bereit und in der Lage ist, zu laufen, angehalten wird, weil das
Betriebssystem beschlossen hat, die CPU eine Zeit lang einem anderen Prozess zuzuweisen. Diese beiden
Bedingungen sind völlig unterschiedlich. Im ersten Fall ist die Unterbrechung dem Problem inhärent
(die Befehlszeile des Benutzers kann erst verarbeitet werden, nachdem sie eingegeben wurde).
Im zweiten Fall handelt es sich um eine technische Besonderheit des Systems (nicht genügend CPUs, um
jedem Prozess einen eigenen Prozessor zu geben).
In Abbildung\ref{fig:02-02} sehen wir ein Zustandsdiagramm, das die drei Zustände zeigt, in denen sich
ein Prozess befinden kann.

\image{02-02}{Übergänge zwischen den Prozess-Zuständen}

Logischerweise sind die ersten beiden Zustände ähnlich. In beiden Fällen ist der Prozess bereit zu laufen,
nur im zweiten Fall ist vorübergehend keine CPU für ihn verfügbar. Der dritte Zustand unterscheidet sich
von den ersten beiden dadurch, dass der Prozess nicht ausgeführt werden kann, auch wenn die CPU nichts
anderes zu tun hat.

Zwischen diesen drei Zuständen sind, wie dargestellt, vier Übergänge möglich. Übergang 1 tritt ein, wenn
ein Prozess feststellt, dass er nicht fortgesetzt werden kann. In einigen Systemen muss der Prozess einen
Systemaufruf ausführen, \texttt{block()} oder \texttt{pause()}, um in den blockierten Zustand zu gelangen.
In anderen Systemen, einschliesslich MINIX 3, wird der Prozess automatisch vom laufenden Zustand in den
blockierten Zustand versetzt, wenn er aus einer Pipe oder einer speziellen Datei liest und keine Eingabe
verfügbar ist.

Die Übergänge 2 und 3 werden vom Prozess-Scheduler, einem Teil des Betriebssystems, verursacht, ohne dass
der Prozess selbst davon weiss. Übergang 2 tritt ein, wenn der Scheduler entscheidet, dass der laufende
Prozess lange genug gelaufen ist und es an der Zeit ist, einem anderen Prozess etwas CPU-Zeit zu geben.
Übergang 3 tritt ein, wenn alle anderen Prozesse ihren Anteil erhalten haben und es an der Zeit ist,
dass der erste Prozess die CPU wieder nutzen kann. Die Entscheidung, welcher Prozess wann und wie lange
laufen soll, ist ein wichtiges Thema der Zeitplanung. Es wurden viele Algorithmen entwickelt, die versuchen,
die konkurrierenden Anforderungen an die Effizienz des Systems als Ganzes und die Fairness gegenüber
einzelnen Prozessen auszugleichen. Im weiteren Verlauf dieses Kapitels werden wir uns mit dem Scheduling
befassen und einige dieser Algorithmen untersuchen.

Übergang 4 tritt ein, wenn das externe Ereignis, auf das ein Prozess gewartet hat (z. B. das Eintreffen einer Eingabe),
eintritt. Wenn zu diesem Zeitpunkt kein anderer Prozess läuft, wird Übergang 3 sofort ausgelöst,
und der Prozess beginnt zu laufen. Andernfalls muss er möglicherweise eine Weile im Bereitschaftszustand
warten, bis die CPU verfügbar ist.

Mit Hilfe des Prozessmodells wird es viel einfacher, sich vorzustellen, was im System vor sich geht.
Einige der Prozesse führen Programme aus, die von einem Benutzer eingegebene Befehle ausführen.
Andere Prozesse sind Teil des Systems und erledigen Aufgaben wie die Ausführung von Anfragen für
Dateidienste oder die Verwaltung der Details des Betriebs einer Festplatte oder eines Bandlaufwerks.
Wenn eine Unterbrechung auf der Festplatte auftritt, kann das System die Entscheidung treffen,
den aktuellen Prozess zu stoppen und den Festplattenprozess auszuführen, der in Erwartung dieser
Unterbrechung blockiert war. Wir sagen `kann', weil dies von den relativen Prioritäten des laufenden
Prozesses und des Plattentreiberprozesses abhängt. Aber der Punkt ist, dass wir statt über Interrupts
über Benutzerprozesse, Plattenprozesse, Terminalprozesse usw. nachdenken können, die blockieren,
wenn sie auf etwas warten, das passieren soll. Wenn der Plattenblock gelesen oder das Zeichen eingegeben
wurde, wird der Prozess, der darauf wartet, freigegeben und kann wieder laufen.

\image{02-03}{Verwaltung der einzelnen Prozesse}

Aus dieser Sicht ergibt sich das in Abbildung~\ref{fig:02-03} dargestellte Modell.
Hier ist die unterste Ebene des Betriebssystems der Scheduler, über dem eine Vielzahl von Prozessen läuft.
Die gesamte Unterbrechungsbehandlung und die Details des tatsächlichen Startens und Stoppens von
Prozessen sind im Scheduler versteckt, der eigentlich recht klein ist. Der Rest des Betriebssystems
ist schön in Form von Prozessen strukturiert.

\subsubsection{Implementierung von Prozessen}

Zur Umsetzung des Prozessmodells unterhält das Betriebssystem eine Tabelle, die so genannte
Prozesstabelle, mit einem Eintrag pro Prozess. Dieser Eintrag enthält Informationen über den
Zustand des Prozesses, seinen Programmzähler, den Stapelzeiger, die Speicherzuweisung,
den Status seiner offenen Dateien, seine Abrechnungs- und Zeitplanungsinformationen,
Alarme und andere Signale und alles andere über den Prozess, das gespeichert werden muss,
wenn der Prozess vom laufenden in den Bereitschaftszustand übergeht, damit er später wieder
gestartet werden kann, als ob er nie angehalten worden wäre.

In MINIX 3 werden die Kommunikation zwischen den Prozessen, die Speicherverwaltung und die
Dateiverwaltung jeweils von separaten Modulen innerhalb des Systems gehandhabt,
so dass die Prozesstabelle partitioniert ist, wobei jedes Modul die Felder pflegt,
die es benötigt. Tabelle~\ref{tab:process-table} zeigt einige Felder.
Die Felder in der ersten Spalte sind die einzigen, die für dieses Kapitel relevant sind.
Die beiden anderen Spalten dienen nur dazu, eine Vorstellung davon zu vermitteln,
welche Informationen an anderer Stelle im System benötigt werden.

\begin{table}[!ht]
    \begin{tabularx}{\textwidth}{p{0.3\textwidth}Xp{0.3\textwidth}}
        \headcell{Kernel}           & \headcell{Prozessmanagement} & \headcell{Dateimanagement} \\
        \addlinespace
        Registers                   & Pointer to text segment      & UMASK mask                 \\ \midrule
        Program counter             & Pointer to data segme        & Root directory             \\ \midrule
        Program status word         & Pointer to bss segment       & Working directory          \\ \midrule
        Stack pointer               & Exit status                  & File descriptors           \\ \midrule
        Process state               & Signal status                & Real id                    \\ \midrule
        Current scheduling priority & Process ID                   & Effective UID              \\ \midrule
        Max. scheduling priority    & Parent process               & Real GID                   \\ \midrule
        Scheduling ticks left       & Process group                & Effective GID              \\ \midrule
        Quantum size                & Children's CPU time          & Controlling tty            \\ \midrule
        CPU time used               & Real UID                     & Save area for r/w          \\ \midrule
        Message queue pointers      & Effective UID                & System call parameters     \\ \midrule
        Pending signal bits         & Real GID                     & Various flag bits          \\ \midrule
    \end{tabularx}
    \caption{Auszug aus der Prozesstabelle}
    \label{tab:process-table}
\end{table}

Nachdem wir nun einen Blick auf die Prozesstabelle geworfen haben, ist es möglich,
ein wenig mehr darüber zu erklären, wie die Illusion mehrerer sequentieller Prozesse
auf einer Maschine mit einer CPU und vielen E/A-Geräten aufrechterhalten wird.
Was folgt, ist technisch gesehen eine Beschreibung, wie der `Scheduler' aus
Abbildung~\ref{fig:02-03} in MINIX 3 funktioniert, aber die meisten modernen
Betriebssysteme arbeiten im Wesentlichen auf die gleiche Weise.
Jeder E/A-Geräteklasse (z. B. Disketten, Festplatten, Timer, Terminals)
ist eine Datenstruktur in einer Tabelle namens Interrupt Descriptor Table
zugeordnet. Der wichtigste Teil jedes Eintrags in dieser Tabelle wird als
Interrupt-Vektor bezeichnet. Er enthält die Adresse der Interrupt-Service-Prozedur.

Nehmen wir an, dass der Benutzerprozess 23 läuft, wenn ein Platteninterrupt auftritt.
Der Programmzähler, das Programmstatuswort und möglicherweise ein oder mehrere
Register werden von der Interrupt-Hardware auf den (aktuellen) Stack geschoben.
Der Computer springt dann zu der im Disk-Interrupt-Vektor angegebenen Adresse.
Das ist alles, was die Hardware tut. Von hier an ist es Sache der Software.

Die Interrupt-Service-Prozedur beginnt mit dem Speichern aller Register im
Prozesstabelleneintrag für den aktuellen Prozess. Die Nummer des aktuellen
Prozesses und ein Zeiger auf seinen Eintrag werden in globalen Variablen
gehalten, damit sie schnell gefunden werden können. Dann werden die von der
Unterbrechung hinterlegten Informationen vom Stack entfernt, und der Stack-Zeiger
wird auf einen temporären Stack gesetzt, der vom Prozesshandler verwendet wird.
Aktionen wie das Speichern der Register und das Setzen des Stack-Zeigers
können nicht einmal in Hochsprachen wie C ausgedrückt werden und werden
daher von einer kleinen Assembler-Routine ausgeführt. Wenn diese Routine fertig
ist, ruft sie eine C-Prozedur auf, um den Rest der Arbeit für diesen speziellen
Unterbrechungstyp zu erledigen.

Hier eine kurze Zusammenfassung, welche Arbeiten das Betriebssystem erledigen muss,
wenn ein Interrupt auftritt.

\begin{enumerate}
    \item Die Harware verschiebt den Programmzähler, etc. auf den Stack
    \item Der neue Programmzähler aus dem Interrupt-Vektor wird durch die Hardware geladen
    \item Eine Assembler-Routine speichert alle Register
    \item Eine Assembler-Routine baut einen neuen Stack auf
    \item Ein <<C>> Interrupt-Service läuft weiter
    \item Der Scheduler entscheidet, welcher Prozess als nächstes zum Zug kommt
    \item Die <<C>> Routine wechselt wieder in den Assembler Code
    \item Die Assembler-Routine startet den neuen Prozess
\end{enumerate}

\subsubsection{Threads}

In herkömmlichen Betriebssystemen hat jeder Prozess einen Adressraum. Das ist eigentlich schon
fast die Definition eines Prozesses. Dennoch gibt es oft Situationen, in denen es
wünschenswert ist, mehrere \emph{Threads} im selben Adressraum zu haben, die quasi
parallel laufen, als wären sie separate Prozesse. Diese Kontroll-Threads werden in
der Regel einfach als Threads bezeichnet, obwohl einige Leute sie als leichtgewichtige
Prozesse bezeichnen.

Eine Möglichkeit, einen Prozess zu betrachten, besteht darin, verwandte Ressourcen
zusammenzufassen. Ein Prozess hat einen Adressraum, der Programmtext und Daten sowie
andere Ressourcen enthält. Zu diesen Ressourcen können offene Dateien,
untergeordnete Prozesse, anstehende Alarme, Signalhandler und vieles mehr gehören.
Indem man sie in Form eines Prozesses zusammenfasst, können sie leichter verwaltet werden.

Ein Thread besitzt einen Befehlszähler, der angibt, welcher Befehl als Nächstes ausgeführt
werden soll. Er besitzt ferner Register, die seine lokalen Variablen beinhalten,
und einen Stack. Dieser spiegelt den vorhergegangenen Ablauf wider, da auf dem
Stack für jede aufgerufene Prozedur, die noch nicht beendet wurde, ein Rahmen (\emph{frame})
liegt. Obwohl ein Thread in einem Prozess laufen muss, sind der Thread und
sein Prozess zwei unterschiedliche Konzepte, die getrennt voneinander behandelt
werden können. Prozesse werden benutzt, um Ressourcen zusammenzufassen---Threads
sind die Einheiten, die für die Ausführung auf der CPU verwaltet werden.


\image{02-11}{(a) Drei Prozesse mit je einem Thread. (b) Ein Prozess mit drei Threads.}

Was Threads dem Prozessmodell hinzufügen, ist die Möglichkeit, dass mehrere
Ausführungen in derselben Prozessumgebung stattfinden können, und zwar
weitgehend unabhängig voneinander.
In Abbildung~\ref{fig:02-11} (a) sehen wir drei traditionelle Prozesse. Jeder
Prozess hat seinen eigenen Adressraum. Im Gegensatz dazu sehen wir in
Abbildung~\ref{fig:02-11} (b) einen einzigen Prozess mit drei \emph{Threads}.
Obwohl wir in beiden Fällen drei Threads haben, arbeitet in (a) jeder von
ihnen in einem anderen Adressraum, während in (b) alle drei denselben
Adressraum teilen.

Ein Beispiel für die Verwendung mehrerer Threads ist ein Webbrowser-Prozess.
Viele Web-Seiten enthalten mehrere kleine Bilder. Für jedes Bild auf einer Webseite
muss der Browser eine separate Verbindung zur Home-Site der Seite aufbauen und das
Bild anfordern. Für den Auf- und Abbau all dieser Verbindungen wird sehr viel
Zeit benötigt. Durch mehrere Threads innerhalb des Browsers können viele Bilder
gleichzeitig angefordert werden, was die Leistung in den meisten Fällen erheblich
beschleunigt, da bei kleinen Bildern die Aufbauzeit der begrenzende Faktor ist,
nicht die Geschwindigkeit der Übertragungsleitung.

\begin{table}[!ht]
    \begin{tabularx}{\textwidth}{p{0.45\textwidth}X}
        \headcell{Prozesseinträge}  & \headcell{Threadeinträge}  \\
        \addlinespace
        Address space               & Program counter            \\ \midrule
        Global variables            & Registers                  \\ \midrule
        Open files                  & Stack                      \\ \midrule
        Child processes             & State                      \\ \midrule
        Pending alarms              &                            \\ \midrule
        Signals and signal handlers &                            \\ \midrule
        Accounting information      &                            \\ \midrule
    \end{tabularx}
    \caption{Elemente, die von allen Threads eines Prozesses gemeinsam genutzt werden}
    \label{tab:thread-table}
\end{table}

Wenn mehrere Threads im gleichen Adressraum vorhanden sind, sind einige der Felder in
Tabelle~\ref{tab:process-table} (Seite~\pageref{tab:process-table}) nicht pro Prozess,
sondern pro Thread, so dass eine separate Thread-Tabelle mit einem Eintrag pro
Thread erforderlich ist. Zu den Elementen pro Thread gehören der Programmzähler,
die Register und der Status. Der Programmzähler wird benötigt, weil Threads,
wie Prozesse, angehalten und wieder aufgenommen werden können. Die Register
werden benötigt, weil die Register der Threads gespeichert werden müssen,
wenn sie angehalten werden. Schliesslich können sich Threads, wie Prozesse,
im laufenden, bereiten oder blockierten Zustand befinden.
In Tabelle~\ref{tab:thread-table} sind einige pro-Prozess- und pro-Thread-Elemente aufgeführt.

% -----------------------------------
\subsection{Interprozesskommunikation}

Prozesse in Computern müssen oft miteinander reden. Zum Beispiel in einer Shell-Pipeline,
wo die Information von einem Prozess zum nächsten weitergegeben wird, bis am Ende
alles erledigt ist. Diese Kommunikation sollte gut organisiert sein, ohne Unterbrechungen.
Wir werden uns mit dem Thema Interprozesskommunikation (IPC) beschäftigen und drei
Hauptprobleme anschauen: Wie Prozesse Informationen austauschen, wie sie vermeiden,
sich gegenseitig zu stören, besonders wenn sie um dieselben Ressourcen konkurrieren,
und wie sie warten, bis ein anderer Prozess fertig ist, wenn sie voneinander abhängig sind.

Diese Probleme betreffen auch Threads, die innerhalb eines Programms laufen.
Threads haben es leichter, Informationen auszutauschen, weil sie denselben Speicher
nutzen. Aber sie müssen auch darauf achten, sich nicht zu stören und in der richtigen
Reihenfolge zu arbeiten, genau wie Prozesse. Wir werden uns diese Themen genauer
ansehen und obwohl wir hauptsächlich über Prozesse sprechen, gelten viele Punkte
auch für Threads.

\subsubsection{Race Conditions}

In manchen Betriebssystemen können Prozesse, die zusammenarbeiten, denselben Speicher
nutzen, um Informationen zu teilen und zu bearbeiten. Dieser gemeinsame Speicher
kann entweder im Hauptspeicher des Computers oder in einer Datei sein.
Wie sie kommunizieren oder welche Probleme auftreten, hängt nicht davon ab,
wo der Speicher liegt. Ein Beispiel für solche Kommunikation ist ein Druckerspooler.
Wenn ein Prozess etwas drucken möchte, fügt er den Namen der Datei in einen speziellen
Ordner ein. Ein Drucker-Programm (Daemon) schaut regelmässig in diesen Ordner,
druckt vorhandene Dateien aus und löscht deren Namen dann.

Stellen wir uns vor, es gibt einen Ordner für den Druckerspooler mit vielen
Einträgen, die nummeriert sind und Dateinamen enthalten können. Es gibt auch
zwei spezielle Werte: \texttt{out}, der auf die nächste zu druckende Datei
zeigt, und \texttt{in}, der auf den nächsten freien Platz im Ordner zeigt.
Diese Werte könnten in einer kleinen Datei gespeichert sein, die für alle
Prozesse zugänglich ist. Wenn zum Beispiel die Einträge 0 bis 3 leer sind
(weil diese Dateien schon gedruckt wurden) und die Einträge 4 bis 6 Dateien
enthalten, die noch gedruckt werden müssen, und wenn zwei Prozesse gleichzeitig
beschliessen, eine Datei zum Drucken hinzuzufügen, nutzen sie diese Informationen.
Diese Situation ist in Abbildung~\ref{fig:02-21} dargestellt.

\image{02-21}{Zwei Prozesse, ein gemeinsamer Speicher}


\subsubsection{Kritische Regionen}

Um Probleme zu vermeiden, die entstehen, wenn mehrere Prozesse gleichzeitig auf
dieselben Daten zugreifen wollen (sogenannte \emph{Race Conditions}), muss man
sicherstellen, dass immer nur ein Prozess zur Zeit diese Daten nutzen kann.
Das nennt man wechselseitigen Ausschluss. Wenn also ein Prozess eine Datei oder
eine Variable benutzt, darf kein anderer Prozess das gleichzeitig tun.
Probleme treten auf, wenn zum Beispiel Prozess B beginnt, eine Variable zu
nutzen, bevor Prozess A fertig ist. Wie man diesen wechselseitigen Ausschluss
erreicht, ist eine wichtige Frage bei der Gestaltung von Betriebssystemen.

\emph{Race Conditions} entstehen nicht, wenn Prozesse für sich arbeiten und
keine gemeinsamen Daten nutzen. Sie treten auf, wenn Prozesse auf gemeinsame
Ressourcen wie Speicher oder Dateien zugreifen müssen. Die Teile eines
Programms, in denen auf gemeinsame Daten zugegriffen wird, nennt man
kritische Bereiche. Wenn man verhindern kann, dass zwei Prozesse gleichzeitig
in ihren kritischen Bereichen arbeiten, kann man Race Conditions vermeiden.

Aber nur \emph{Race Conditions} zu vermeiden, reicht nicht aus, um
sicherzustellen, dass Prozesse, die gemeinsame Daten nutzen, gut und
effizient zusammenarbeiten. Es gibt vier wichtige Bedingungen, die
erfüllt sein müssen, um eine gute Lösung für das Zusammenarbeiten
mit gemeinsamen Daten zu finden.

\begin{enumerate}
    \item Keine zwei Prozesse dürfen gleichzeitig in ihren kritischen Regionen sein.
    \item Es dürfen keine Annahmen über Geschwindigkeit und Anzahl der CPUs gemacht
        werden.
    \item Kein Prozess, der ausserhalb seiner kritischen Region läuft, darf
        andere Prozesse blockieren.
    \item Kein Prozess sollte ewig darauf warten müssen, in seine kritische Region einzutreten.
\end{enumerate}

\image{02-22}{Wechselseitiger Ausschluss unter Verwendung von kritischen Regionen}


%\subsection{Klassische IPC-Probleme}
%\subsection{Scheduling}
%\subsection{Überblick über die Prozesse in MINIX 3}
%\subsection{Implementierung von Prozessen in MINIX 3}


\input{chapters/02processes-exercises.tex}
