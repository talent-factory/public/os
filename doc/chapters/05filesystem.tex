\section{Dateisystem}
\label{sec:dateisystem}

\subsection{Dateien}

Dateien sind im Kern digitaler Speichermedien von grundlegender Bedeutung. Sie ermöglichen es,
Daten langfristig und über die Laufzeit einzelner Prozesse hinaus zu speichern, was wiederum
den Zugriff und die Manipulation dieser Daten durch verschiedene Prozesse und Benutzer
gestattet. In diesem Zusammenhang führen die folgenden Abschnitte in die Konzepte, Strukturen,
Typen, Operationen und die Verwaltung von Dateien ein.

\subsubsection{Benennung von Dateien}

In modernen Betriebssystemen spielt die Art und Weise, wie Dateien benannt werden, eine
entscheidende Rolle bei der Organisation und Verwaltung von Daten. Die Möglichkeit,
Dateien bestimmte Namen zuzuweisen, ermöglicht es Benutzern und Programmen, auf diese
Daten effizient zuzugreifen. Die Konventionen und Regeln, die bei der Benennung von
Dateien befolgt werden müssen, sind wesentliche Bestandteile der Dateisysteme und
variieren zwischen verschiedenen Betriebssystemen.

% \subsubsection{Grundregeln}

Die Benennung einer Datei folgt in den meisten Betriebssystemen bestimmten Grundregeln,
die die Verwendung von Buchstaben, Zahlen und einigen Sonderzeichen erlauben.
Eine allgemeine Beschränkung besteht in der maximalen Länge eines Dateinamens,
die oft auf 255 Zeichen begrenzt ist. Diese Begrenzung ermöglicht eine ausreichende
Deskriptivität, während sie gleichzeitig effiziente Such- und Verwaltungsvorgänge
unterstützt.

Viele Betriebssysteme unterstützen zweigeteilte Dateinamen, wobei die beiden Teile
durch einen Punkt voneinander getrennt werden, wie z.B.\ bei \texttt{prog.c}. Der Teil
hinter dem Punkt wird Dateiendung oder Dateinamenserweiterung ({\em file extension})
genannt und enthält in der Regel bestimmte Informationen über die Datei.

Unter UNIX ist die Länge der Erweiterung, falls es überhaupt eine gibt, dem Benutzer
überlassen. Hier kann eine Datei sogar zwei oder mehr Erweiterungen haben, wie in
der Bezeichnung \texttt{homepage.html.zip}, wobei \texttt{.html} anzeigt, dass es
sich um eine Webseite in HTML handelt, und \texttt{.zip}, dass die Datei
\texttt{homepage.html} mit einem ZIP-Programm komprimiert wurde.

\image{04-01}{Einige typische Dateiendungen}

% \subsubsection{Gross- und Kleinschreibung}

Ein weiterer wichtiger Aspekt bei der Benennung von Dateien ist die Berücksichtigung
der Gross- und Kleinschreibung. UNIX-basierte Systeme unterscheiden üblicherweise
zwischen Gross- und Kleinschreibung in Dateinamen, was bedeutet, dass „Beispiel.txt“
und „beispiel.txt“ als zwei separate Dateien behandelt werden. Im Gegensatz dazu
machen Systeme wie Windows keinen Unterschied zwischen Gross- und Kleinschreibung
in Dateinamen, was zu einer vereinfachten Dateiverwaltung führt, aber auch zu
Verwechslungen führen kann, wenn Dateien von einem Betriebssystem zum anderen
übertragen werden.

% \subsubsection{Bedeutung für die Anwendungsentwicklung}

Für Entwickler ist die Kenntnis dieser Konventionen der Dateibenennung essenziell,
um plattformübergreifende Kompatibilität zu gewährleisten. Die Wahl eines Dateinamens
kann Auswirkungen auf die Auffindbarkeit und den Zugriff auf die Datei haben,
insbesondere in Umgebungen mit einer grossen Anzahl von Dateien und Verzeichnissen.
Darüber hinaus kann die Einhaltung von klaren Konventionen bei der Benennung von
Dateien dazu beitragen, Verwirrung zu vermeiden und die Handhabung von Daten zu
erleichtern.


\subsubsection{Dateistruktur}

Die Struktur von Dateien ist ein fundamentales Konzept, das die Art und Weise definiert,
wie Daten innerhalb einer Datei organisiert und gespeichert werden. In modernen
Betriebssystemen werden verschiedene Dateistrukturen unterstützt, um unterschiedlichen
Anforderungen gerecht zu werden. Die einfachste und am weitesten verbreitete Form
ist die unstrukturierte Datei, die eine sequenzielle Abfolge von Bytes darstellt.
Diese Form bietet maximale Flexibilität, da sie keinerlei Vorgaben bezüglich der
Datenorganisation macht und somit für eine Vielzahl von Anwendungen geeignet ist.
Drei der gebräuch- lichsten Möglichkeiten sind in Abbildung\ref{fig:04-02}\ dargestellt.

\image{04-02}{Drei Dateiarten (a) Byte-Folge (b) Folge von Datensätzen (c) Baum}

Darüber hinaus existieren strukturierte Formate, die Daten in festen oder variablen
Längen organisieren, um den Zugriff und die Verarbeitung zu erleichtern. Diese
Organisationsformen reichen von einfachen Zeilen oder Datensätzen bis hin zu
komplexeren Strukturen wie Bäumen oder Graphen, die schnelle Such- und Sortieroperationen
ermöglichen.

Die Wahl der Dateistruktur hat direkte Auswirkungen auf die Performance von
Anwendungen, besonders wenn es um das Lesen oder Manipulieren grosser Datenmengen geht.
Jede Struktur hat ihre spezifischen Vor- und Nachteile, wobei die Entscheidung
für eine bestimmte Struktur von den Anforderungen der zu speichernden und zu
verarbeitenden Daten abhängt.

\subsubsection{Dateitypen}

Die Klassifizierung von Dateien nach ihrem Typ spielt eine entscheidende Rolle
bei der Verwaltung und Verwendung von Daten. Dateitypen werden häufig durch
Dateiendungen angezeigt, die ein Hinweis auf den Inhalt und das Format der
Datei geben. Beispielsweise deutet die Endung \texttt{.txt} auf eine Textdatei hin,
während \texttt{.jpg} und \texttt{.png} Bildformate kennzeichnen.

In Betriebssystemen wie UNIX werden Dateiendungen eher als Konvention denn
als strikte Regel behandelt, und die Eigenschaften einer Datei werden durch
ihre Zugriffsrechte und Meta-Informationen definiert. Im Gegensatz dazu
nutzen Betriebssysteme wie Windows oder MacOS Dateitypen, um Anwendungen
mit bestimmten Dateiformaten zu verknüpfen und diese durch einfaches
Doppelklicken zu öffnen.

Die Unterscheidung von Dateitypen erleichtert die Organisation von Daten
und ermöglicht es Betriebssystemen und Anwendungen, spezifische Funktionen
für den Umgang mit verschiedenen Dateiformaten bereitzustellen. Dies
unterstützt eine effiziente Verwaltung und Nutzung von Ressourcen.


Der Header (siehe Abbildung\ \ref{fig:04-03}, Seite~\pageref{fig:04-03}) beginnt mit einer sogenannten
magischen Zahl\footnote{\href{https://en.wikipedia.org/wiki/List_of_file_signatures}
{https://en.wikipedia.org/wiki/List\_of\_file\_signatures}}
({\em magic number}), die die Datei als ausführbar kennzeichnet.
Diese magische Zahl ist jedoch nicht nur auf ausführbare Dateien anwendbar.
Die Anwendung von magischen Zahlen stellt ein wichtiges Instrument zur
Typisierung und Sicherstellung der Integrität von Daten und Dateisystemen
dar. Sie ermöglichen eine schnelle und effiziente Identifikation von
Dateiformaten und Dateisystemstrukturen, was besonders beim Starten von
Betriebssystemen, beim Laden von Programmen und bei der Fehlerbehebung
in Dateisystemen von Bedeutung ist.

\image{04-03}{(a) Eine ausführbare Datei (b) Ein Archiv}

\subsubsection{Dateizugriff}

Der Zugriff auf Dateien ist eine grundlegende Funktion von Betriebssystemen,
die durch verschiedene Operationen wie Lesen, Schreiben, Öffnen und Schliessen
von Dateien realisiert wird. Betriebssysteme bieten Schnittstellen an,
durch die Anwendungen auf Dateien zugreifen und mit ihnen interagieren
können. Diese Schnittstellen abstrahieren die Details des physischen
Speichermediums und ermöglichen es Anwendungen, Daten konsistent und
sicher zu behandeln.

Die Art des Dateizugriffs kann sequenziell oder zufällig sein, wobei
sequenzieller Zugriff bedeutet, dass Daten in der Reihenfolge ihres
Erscheinens gelesen oder geschrieben werden, während zufälliger Zugriff
den direkten Zugriff auf spezifische Datenpositionen erlaubt. Diese
Zugriffsarten unterstützen verschiedene Anwendungsanforderungen und
haben jeweils unterschiedliche Auswirkungen auf die Performance.

\subsubsection{Dateiattribute}

Dateiattribute sind Meta-Informationen, die zusätzliche Eigenschaften über
Dateien speichern, wie beispielsweise Zugriffsrechte, Erstellungs- und
Modifikationszeitstempel, Dateigrösse und spezielle Markierungen. Diese
Attribute sind essentiell für die Verwaltung von Dateien in einem
Betriebssystem, da sie steuern, wer Zugriff auf eine Datei hat, wie
Dateien behandelt werden sollen und welche Operationen darauf ausgeführt
werden können.

Zugriffsrechte sind besonders wichtig in Mehrbenutzersystemen, wo sie
festlegen, welche Benutzer oder Gruppen Dateien lesen, schreiben oder
ausführen dürfen. Andere Attribute, wie Archiv-Bits, geben an, ob eine
Datei gesichert werden muss oder bereits gesichert wurde, was für Backup-
und Wiederherstellungsprozesse von Bedeutung ist.

\subsubsection{Dateioperationen}

Die Manipulation von Dateien durch Operationen wie Erstellen, Löschen,
Kopieren, Verschieben und Umbenennen ist eine zentrale Funktionalität
von Betriebssystemen. Diese Operationen ermöglichen es Benutzern und
Programmen, Daten effektiv zu organisieren und zu verwalten. Zusätzlich
zu diesen grundlegenden Operationen bieten viele Betriebssysteme auch
erweiterte Funktionen wie das Festlegen von Zugriffsrechten, das Ändern
von Attributen und das Überwachen von Dateiänderungen.

Die Durchführung dieser Operationen wird durch Betriebssystem-APIs
erleichtert, die eine abstrakte und einheitliche Schnittstelle für
die Interaktion mit dem Dateisystem bieten. Dieses hohe Abstraktionsniveau
ermöglicht es Anwendungen, unabhängig von den zugrundeliegenden
Dateisystemdetails zu arbeiten.

% ==========

\subsection{Verzeichnisse}

Dateisysteme verwenden Verzeichnisse oder Ordner, um Dateien zu organisieren
und zu verwalten. In vielen Systemen werden Verzeichnisse selbst als spezielle
Dateien behandelt. In diesem Abschnitt werden die Struktur, Eigenschaften
und möglichen Operationen von Verzeichnissen besprochen.

\subsubsection{Verzeichnisse mit einer Ebene}

Die einfachste Form eines Verzeichnissystems ist ein einzelnes Verzeichnis,
das alle Dateien enthält. Dies wird manchmal als Wurzelverzeichnis bezeichnet.
Auf frühen Personalcomputern war dieses System üblich, da es meist nur einen
Benutzer gab. Auch der erste Supercomputer, der CDC 6600, nutzte ein einziges
Verzeichnis für alle Dateien, trotz der gleichzeitigen Nutzung durch viele
Benutzer, um den Softwareentwurf einfach zu halten.

\imagenew{04-06}{Eine Ebene und vier Dateien}{6cm}

Ein System mit nur einem Verzeichnis, wie in Abbildung~\ref{fig:04-06} gezeigt,
enthält vier Dateien. Vorteile dieses Modells sind die Einfachheit und das
schnelle Auffinden von Dateien, da es nur einen Suchort gibt. Solche
Systeme werden oft in eingebetteten Geräten wie Telefonen, Digitalkameras
und tragbaren Musik-Playern verwendet.

\subsubsection{Hierarchische Verzeichnissysteme}

Ein Verzeichnissystem mit einer Ebene ist für einfache Anwendungen ausreichend
und wurde auf frühen PCs verwendet. Heute haben Benutzer jedoch Tausende
von Dateien, und ein einzelnes Verzeichnis wäre unpraktisch. Es ist notwendig,
zusammenhängende Dateien zu gruppieren.

\image{04-07}{Hierarchisches Verzeichnis}

Es wird eine Hierarchie, wie ein Verzeichnisbaum, benötigt. Mit diesem Ansatz
können beliebig viele Verzeichnisse erstellt werden, um Dateien sinnvoll
zu ordnen. In Unternehmensnetzwerken mit gemeinsamen Dateiservern können
alle Benutzer ihr eigenes privates Wurzelverzeichnis für ihre eigene Hierarchie
einrichten. In Abbildung~\ref{fig:04-07} wird gezeigt, dass die
Verzeichnisse A, B und C im Wurzelverzeichnis verschiedenen Benutzern
gehören. Zwei dieser Benutzer haben Unterverzeichnisse für ihre Projekte erstellt.

Die Möglichkeit, beliebig viele Unterverzeichnisse zu erstellen, bietet den
Benutzern ein mächtiges Werkzeug zur Strukturierung ihrer Arbeit. Daher sind
fast alle modernen Dateisysteme auf diese Weise organisiert.

\subsubsection{Pfadnamen}

In einem als Verzeichnisbaum organisierten Dateisystem gibt es zwei Methoden,
um Dateinamen zu spezifizieren. Die erste Methode verwendet absolute Pfadnamen,
die den gesamten Pfad von der Wurzel bis zur Datei umfassen. Beispielsweise
bedeutet der Pfadname \cmd{/usr/ast/mailbox}, dass im Wurzelverzeichnis ein
Unterverzeichnis usr existiert, das wiederum ein Unterverzeichnis ast enthält,
in dem sich die Datei mailbox befindet. Absolute Pfadnamen beginnen immer mit
dem Wurzelverzeichnis und sind eindeutig. In UNIX werden Pfadbestandteile
durch das Zeichen \texttt{$\slash $} getrennt, in Windows durch \texttt{$\backslash$}.

Die zweite Methode ist der relative Pfadname, der in Verbindung mit dem
Arbeitsverzeichnis (aktuelles Verzeichnis) verwendet wird. Ein Benutzer
kann ein Verzeichnis als aktuelles Arbeitsverzeichnis festlegen, wodurch
alle Pfadnamen, die nicht mit dem Wurzelverzeichnis beginnen, relativ zu
diesem Arbeitsverzeichnis betrachtet werden. Beispielsweise kann bei
einem aktuellen Arbeitsverzeichnis \cmd{/usr/ast} auf die Datei \cmd{/usr/ast/mailbox}
einfach mit \cmd{mailbox} zugegriffen werden.

Die meisten Systeme mit hierarchischer Verzeichnisstruktur haben zwei spezielle
Einträge in jedem Verzeichnis: cmd{.} und cmd{..}. cmd{.} bezieht sich auf das aktuelle
Verzeichnis und cmd{..} auf das übergeordnete Verzeichnis (ausser im Wurzelverzeichnis,
wo cmd{..} auf sich selbst verweist). Diese Einträge ermöglichen es, im
Verzeichnisbaum zu navigieren. Beispielsweise kann ein Prozess mit dem
Arbeitsverzeichnis /usr/ast cmd{..} verwenden, um im Verzeichnisbaum nach oben
zu gelangen.

\image{04-08}{Unix Verzeichnisbaum}

\subsubsection{Operationen auf Verzeichnissen}

Die Systemaufrufe zur Verwaltung von Verzeichnissen variieren stärker zwischen
den Systemen als die Systemaufrufe für Dateien. Um einen Eindruck von den
möglichen Operationen und ihrer Funktionsweise zu vermitteln, wird im
Folgenden eine Auswahl aus der UNIX-Welt vorgestellt.

\begin{description}
    \item[Create] Ein Verzeichnis wird erstellt und ist zunächst leer, abgesehen
    von den beiden automatisch erstellten Einträgen \cmd{.} und \cmd{..}. Diese
    Einträge werden vom System automatisch angelegt, oder in einigen Fällen durch
    das \cmd{mkdir}-Programm.

    \item[Delete] Beim Löschen eines Verzeichnisses muss es leer sein, was bedeutet,
    dass es nur die Einträge \cmd{.} und \cmd{..} enthalten darf. Diese beiden
    Einträge allein gelten als leer, da sie normalerweise nicht gelöscht werden können.

    \item[Opendir] Um Verzeichnisse zu lesen, öffnet ein Programm das Verzeichnis und
    liest die Namen aller darin enthaltenen Dateien. Vor dem Lesen muss das Verzeichnis,
    ähnlich wie bei Dateien, geöffnet werden.

    \item[Closedir]Nachdem ein Verzeichnis gelesen wurde, sollte es anschliessend geschlossen
    werden, um den internen Tabellenspeicher freizugeben.

    \item[Readdir] Der Aufruf \cmd{readdir} gibt den nächsten Eintrag eines geöffneten
    Verzeichnisses zurück. Früher war es möglich, Verzeichnisse über den gewöhnlichen
    \cmd{read}-Systemaufruf auszulesen, was jedoch erforderte, dass der Programmierer
    die interne Struktur der Verzeichnisse kannte und damit umgehen konnte. Im Gegensatz
    dazu liefert \cmd{readdir} immer einen Eintrag in einem standardisierten Format zurück,
    unabhängig von der verwendeten Verzeichnisstruktur.

    \item[Rename] Verzeichnisse verhalten sich in vielerlei Hinsicht wie Dateien und können
    auch umbenannt werden.

    \item[Link] Durch die Verlinkungstechnik können Dateien in mehreren Verzeichnissen
    existieren. Dieser Aufruf spezifiziert eine vorhandene Datei und erstellt eine
    Verbindung von dieser Datei zu dem Namen, der durch den angegebenen Pfad bestimmt
    wird. Dadurch kann dieselbe Datei in mehreren Verzeichnissen vorkommen. Diese Art
    von Verbindung, die den Zähler im I-Node der Datei erhöht, wird manchmal als
    harter Link bezeichnet.

    \item[Unlink] eim Entfernen eines Verzeichniseintrags wird, wenn die Datei nur
    in einem Verzeichnis vorhanden ist, diese Datei aus dem Dateisystem entfernt.
    Ist die Datei jedoch in verschiedenen Verzeichnissen vorhanden, wird sie nur
    unter dem angegebenen Pfad gelöscht, während andere Versionen erhalten bleiben.
    In UNIX ist der Systemaufruf zum Löschen von Dateien tatsächlich \cmd{unlink}.

\end{description}

Symbolische Links sind eine Variante des Dateiverknüpfungskonzepts.
Anstelle von zwei Namen, die auf dieselbe interne Datenstruktur zeigen,
wird ein Name erzeugt, der auf eine kleine Datei verweist, welche wiederum
den Namen einer anderen Datei enthält. Wenn die erste Datei verwendet wird,
folgt das Dateisystem dem Pfad und findet den Namen der zweiten Datei.
Symbolische Links haben den Vorteil, Plattengrenzen zu überschreiten
und sogar Dateien auf entfernten Computern anzusprechen. Ihre Implementierung
ist jedoch etwas weniger effizient als bei harten Links.

\subsection{Layout eines Dateisystems}

Dateisysteme werden auf Platten gespeichert, die oft in eine oder mehrere Partitionen
unterteilt sind, wobei jedes eine eigenständige Dateisystemstruktur aufweist.
Der Sektor 0 auf der Platte ist der MBR ({\em Master Boot Record}), der beim
Hochfahren des Computers verwendet wird und eine Partitionstabelle enthält.
Eine Partition ist als aktiv markiert. Das BIOS liest den MBR beim Starten ein
und führt ihn aus. Das MBR-Programm lokalisiert die aktive Partition und führt
deren Boot-Block aus, der das Betriebssystem lädt. Jede Partition beginnt mit
einem Boot-Block aus Gründen der Einheitlichkeit, auch wenn sie kein bootfähiges
Betriebssystem enthält.

Das Layout der Partitionen variiert je nach Dateisystem. Oft enthält das
Dateisystem den Superblock, der wichtige Parameter enthält und beim Starten
des Computers oder beim ersten Zugriff auf das Dateisystem geladen wird.
Der Superblock enthält typischerweise Informationen wie eine magische
Zahl zur Identifizierung des Dateisystemtyps, die Anzahl der Blöcke im
Dateisystem und andere administrative Details.

\image{04-09}{Mögliches Layout}

Die Informationen im Dateisystem könnten wie folgt strukturiert sein:
Zuerst könnten Details zu den freien Blöcken kommen, möglicherweise
in Form einer Bitmap oder einer Liste von Zeigern. Anschließend könnten
die I-Nodes folgen, die eine Datenstruktur darstellen, die alle
Informationen über jede Datei enthält. Danach könnte das Wurzelverzeichnis
kommen, welches die Spitze des Dateibaumes darstellt. Der Rest der
Platte enthält in der Regel alle anderen Verzeichnisse und Dateien.

% ==========

% ==========

%\subsection{Beispiele von Dateisystemen}

% ==========

\clearpage
\subsection*{Übungen}

\begin{Exercise}[title={Anwendung von Dateiendungen in verschiedenen Betriebssystemen}]

    Erklären Sie, wie unterschiedliche Betriebssysteme (z.B.\ UNIX im Vergleich zu Windows)
    mit Dateiendungen umgehen und welche Implikationen dies für die Entwicklung
    plattformübergreifender Software hat. Welche Strategien könnten Entwickler
    anwenden, um eine möglichst hohe Kompatibilität ihrer Anwendungen sicherzustellen?
\end{Exercise}

\begin{Answer}
    Unter UNIX werden Dateiendungen eher als Konvention denn als strikte Regel behandelt,
    wohingegen Windows-Systeme Dateiendungen verwenden, um Dateitypen bestimmten
    Anwendungen zuzuordnen. Diese unterschiedliche Behandlung erfordert von Entwicklern,
    dass sie sich besonders um die Benennung Ihrer Dateien kümmern, um
    Kompatibilitätsprobleme zu vermeiden. Strategien für Entwickler könnten sein:
    Verwendung eindeutiger und sinnvoller Dateiendungen, Bereitstellung von
    Konfigurationsoptionen für die Benutzer zur Verwaltung von Dateiassoziationen
    und die Implementierung eigener Mechanismen zur Erkennung von Dateiinhalten
    unabhängig von deren Endungen.
\end{Answer}

% ---

\begin{Exercise}[title={Effizienz von Dateistrukturen}]

    Vergleichen Sie unstrukturierte und strukturierte Dateiformate in Bezug auf ihre
    Effizienz beim Zugriff auf Daten. Unter welchen Bedingungen könnte eine
    unstrukturierte Datei einem strukturierten Format vorzuziehen sein und
    umgekehrt? Beziehen Sie sich auf konkrete Anwendungsfälle.
\end{Exercise}

\begin{Answer}
    Unstrukturierte Dateien, die einfach eine Byte-Sequenz speichern, sind besonders
    für binäre Daten oder Situationen geeignet, in denen der Zugriff auf die
    gesamte Datei als Ganzes erfolgt. Strukturierte Formate sind vorteilhaft,
    wenn Daten in einer vorgegebenen Weise verarbeitet und effizient in Teilen
    gelesen oder geschrieben werden sollen. Beispielsweise könnten Datenbanken
    oder Konfigurationsdateien von einem strukturierten Format profitieren.
\end{Answer}

% ---

\begin{Exercise}[title={Erkennung von Dateitypen ohne Endungen}]

    In Unix- und Linux-Systemen spielen Dateiendungen eine geringere Rolle
    als in Windows. Beschreiben Sie, wie das System stattdessen Dateitypen
    erkennen und entsprechend verarbeiten kann. Welche Tools oder
    Kommandos könnten dabei eine Rolle spielen?
\end{Exercise}

\begin{Answer}
    Systeme wie Unix/Linux nutzen Tools wie \texttt{file}, das die magischen Zahlen
    einer Datei liest, um ihren Typ unabhängig von der Dateiendung zu bestimmen.
    Dies ermöglicht eine flexible Handhabung verschiedener Dateiformate und
    deren Inhalte. Programme oder Skripte können ebenfalls mit Hilfe dieser
    Technik entwickelt werden, um den Inhalt von Dateien zu analysieren und
    entsprechend zu verarbeiten.
\end{Answer}

% ---

\begin{Exercise}[title={Dateibenennung und -verwaltung}]

    Erstellen Sie ein Skript, das eine Liste von Dateien in einem Verzeichnis
    erstellt. Die Dateinamen sollten dabei bestimmten Regeln folgen
    (z.B.\ maximal 255 Zeichen, Verwendung von Buchstaben, Zahlen und bestimmten
    Sonderzeichen). Prüfen Sie, ob die Dateinamen den Regeln entsprechen
    und benennen Sie sie gegebenenfalls um.
\end{Exercise}

\begin{Answer}
    Das folgende Script konzentriert sich auf den zweiten Teil der Aufgabe:
    Überprüfen der Regeln und umbenennen der Datei, falls der Name nicht den
    Regelnt entsprich. Verwendet wird an dieser Stelle ein
    Regulärer\footnote{Zur Überprüfung von regulären Ausdrücken kann die folgende
    Webseite genutzt werden: \href{https://regexr.com/}{https://regexr.com/}.}
    Ausdruck

    \src{python}{src/exercise/}{check-and-rename-files.py}
\end{Answer}

\begin{Exercise}[title={Unterschiedliche Dateiendungen}]

    Schreiben Sie ein Programm, das eine Liste von Dateien mit verschiedenen Endungen
    erstellt (z.B.\ .txt, .jpg, .png). Implementieren Sie eine Funktion,
    die diese Dateien nach ihren Endungen sortiert und in entsprechende
    Unterverzeichnisse verschiebt.
\end{Exercise}
