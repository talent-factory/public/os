\section{Standard I/O}
\label{sec:io}


% Viele Programme lesen nur eine Eingabe und schreiben eine Ausgabe. Dieser Vorgang
% kann wie in Abbildung~\ref{fig:io} dargestellt werden.

% \image{io}{Standard I/O}

\subsection{Umleitung von Eingabe und Ausgabe}

Im Grunde gibt es keinen Unterschied zwischen dem Lesen von Daten aus einer Datei
und dem Lesen von Daten von einem Terminal. Ebenso gibt es keinen Unterschied
zwischen dem Schreiben in eine Datei, dem Schreiben in ein Terminal und dem
Schreiben in die Eingabe eines anderen Programms, solange die Ausgabe nur
aus alphanumerischen Zeichen und Interpunktion besteht. Die Standard-E/A-Funktion
bietet einige einfache Standardeinstellungen für die Verwaltung von Ein- und
Ausgaben und definiert drei Standard-E/A-Ströme: Standardeingabe (\emph{stdin}),
Standardausgabe (\emph{stdout}) und Standardfehler (\emph{stderr}).

Die Standardeingabe kommt normalerweise von der Tastatur, die Standardausgabe
und der Standardfehler gehen an das Terminal. Programme lesen die Standardeingabe
in der Regel von der Tastatur, es sei denn, die Shell leitet sie von einer Datei
um. Dies ist nützlich für Programme, die keine Dateien direkt öffnen können.
Die Shell kann die Standardeingabe von einer Datei umleiten, was besonders
für Programme wie \cmd{mail} praktisch ist, die Dateien nicht direkt öffnen können.

\begin{minted}{sh}
    $ mail daniel < inhalt.txt
\end{minted}

Der eigentliche Vorteil von Standard-E/A besteht darin, dass Eingaben oder
Ausgaben von Ihrem Terminal in eine Datei umgeleitet werden können.
Da Unix dateibasiert ist und Terminals sowie andere E/A-Geräte wie Dateien
behandelt, muss ein Programm nicht wissen, ob es seine Ausgabe an ein
Terminal oder an eine Datei sendet. Zum Beispiel können Sie die Ausgabe
des Befehls \texttt{cat file1 file2} in \texttt{Datei3} umleiten, anstatt sie an
Ihr Terminal zu schicken, indem Sie den entsprechenden Befehl eingeben:

\begin{minted}{sh}
    $ cat file1 file2 > file3
\end{minted}

Dies wird als Umleitung der Standardausgabe bezeichnet. Wenn Sie diesen
Befehl verwenden und \texttt{file3} ansehen, finden Sie den Inhalt von \texttt{file1},
gefolgt vom Inhalt von \texttt{file2} - genau das, was Sie auf Ihrem Bildschirm
gesehen hätten, wenn Sie den Modifikator \texttt{> file3} weggelassen hätten.
(Die Z-Shell\footnote{\href{https://zsh.sourceforge.io/}{https://zsh.sourceforge.io/};
siehe auch \cite{zsh}.}
geht mit der Umleitung mehrerer Dateien noch weiter).

Standard-E/A ist zwar ein grundlegendes Merkmal von Unix, aber die Syntax, die zur Umleitung von
Standard-E/A umzuleiten, hängt von der verwendeten Shell ab. Die Syntax der Bourne-Shell und der C
Shell-Syntax unterscheiden sich, vor allem wenn es um die weniger gebräuchlichen
Funktionen. Die Korn-Shell und die Bash sind mit der Bourne-Shell identisch, aber mit
ein paar eigenen Wendungen. Die Z-Shell versteht im Allgemeinen beide Syntaxen
(und fügt in gewohnter Manier noch mehr hinzu).

\begin{table}[!ht]
    \begin{tabularx}{\textwidth}{Xp{0.20\textwidth}p{0.20\textwidth}}
        \headcell{Funktion}                                        & \headcell{csh}          & \headcell{sh}               \\
        \addlinespace
        \emph{stdout} an eine Datei senden                         & \texttt{prog > file}    & \texttt{prog > file}        \\ \midrule
        \emph{stderr} an eine Datei senden                         &                         & \texttt{prog 2> file}       \\ \midrule
        \emph{stdout} und \emph{stderr} an eine Datei senden       & \texttt{prog >\& file}  & \texttt{prog > file 2>\&1}  \\ \midrule
        \emph{stdin} aus Datei übernehmen                          & \texttt{prog < file}    & \texttt{prog < file}        \\ \midrule
        \emph{stdout} an das Ende der Datei senden                 & \texttt{prog >> file}   & \texttt{prog >> file}       \\ \midrule
        \emph{stderr} an das Ende der Datei senden                 &                         & \texttt{prog 2>> file}      \\ \midrule
        \emph{stdout} und \emph{stderr} ans Ende der Datei senden  & \texttt{prog >>\& file} & \texttt{prog >> file 2>\&1}  \\ \midrule
        \emph{stdin} von der Tastatur lesen bis EOF                & \texttt{prog << EOF}    & \texttt{prog << EOF}        \\ \midrule
        \emph{stdout} nach \texttt{prog2} leiten                   & \texttt{prog | prog2}   & \texttt{prog | prog2}       \\ \midrule
        \emph{stdout} und \emph{stderr} nach \texttt{prog2} leiten & \texttt{prog |\& prog2} & \texttt{prog 2>\&1 | prog2} \\ \midrule
    \end{tabularx}
    \caption{Gängige Standard-E/A-Umleitungen}
    \label{tab:io-redirection}
\end{table}

% =======
\input{chapters/06io-exercises01}
% =======

\subsection{Programmieren mit Standard E/A}

Neue Programme von Grund auf zu schreiben, kann sinnvoll sein, wenn bestehende
Programme nicht ausreichen, besonders bei der Verarbeitung von Nicht-Text-Dateien.
Mit der Shell und anderen allgemeinen Werkzeugen ist es oft schwer, ausreichende
Robustheit oder Effizienz zu erreichen. Eine Shell-Version kann helfen, die
Programmdefinition und Benutzeroberfläche zu verfeinern, aber oft ist eine
Neuentwicklung in C notwendig.

Wir programmieren in C, weil es die Standardsprache für UNIX-Systeme ist und
am besten unterstützt wird. Grundkenntnisse in C werden vorausgesetzt. Siehe
hierzu auch Abschnitt~\ref{sec:c-introduction} (Seite~\pageref{sec:c-introduction}).

Wir nutzen die Standard-I/O-Bibliothek, die effiziente und portable Ein-/Ausgabe-
und Systemdienste für C-Programme bietet. Diese Bibliothek ist auch auf vielen
(auch Nicht-UNIX-Systemen) verfügbar, was die Portabilität erleichtert.

\subsubsection{Standardeingabe und -ausgabe: \cmd{vis}}

Zur Veranschaulichung ein Programm namens \cmd{vis}, das die Standardeingabe in
die Standardausgabe kopiert, mit dem Unterschied, dass es alle nicht druckbaren
Zeichen sichtbar macht, indem es sie als \verb|\000| ausgibt, wobei nnn der
Oktalwert des Zeichens ist. vis ist von unschätzbarem Wert, um seltsame oder
unerwünschte Zeichen aufzuspüren, die sich in Dateien eingeschlichen haben.
Zum Beispiel druckt \texttt{vis} jedes Backspace-Zeichen als \verb|\010| aus,
was dem oktalen Wert (siehe Tabelle~\ref{tab:ascii}, Seite~\pageref{tab:ascii})
des Backspace-Zeichens entspricht:

\begin{minted}{text}
    $ cat vis.input
    a
    $ ./vis < vis.input
    abc\010\010
\end{minted}

Zur Erstellung der Testdatei \texttt{vis.input} wurde das
Programm \cmd{printf}\footnote{%
    \href{https://man7.org/linux/man-pages/man1/printf.1.html}%
    {https://man7.org/linux/man-pages/man1/printf.1.html}}
wie folgt angewendet:

\begin{minted}{sh}
    $ printf 'abc\010\010' > vis.input
\end{minted}

Um mehrere Dateien mit dieser rudimentären Version von \texttt{vis} zu scannen,
können wir einfach \cmd{cat} verwenden, um die Dateien zu sammeln:

\begin{minted}{sh}
    $ cat file1 file2 ... | vis
    ...
    $ cat file1 file2 ... | vis | grep '\\'
\end{minted}

Die einfachsten Ein- und Ausgaberoutinen heissen \texttt{getchar} und \texttt{putchar}.
Jeder Aufruf von \texttt{getchar} holt das nächste Zeichen von der Standardeingabe,
die eine Datei, eine Pipe oder das Terminal sein kann. In ähnlicher Weise gibt
\texttt{putchar(c)} das Zeichen \texttt{c} auf der Standardausgabe aus, die
standardmässig ebenfalls das Terminal ist.

Die Funktion \texttt{printf} übernimmt die Umwandlung des Ausgabeformats. Die Aufrufe
von \texttt{printf} und \texttt{putchar} können in beliebiger Reihenfolge erfolgen;
die Ausgabe erfolgt in der Reihenfolge der Aufrufe.

Hier ist die erste Version von \cmd{vis}:

\src{c}{src/examples/}{vis.1.c}

\texttt{getchar} gibt das nächste Byte aus der Eingabe zurück oder den Wert \texttt{EOF},
wenn es auf das Ende der Datei (oder einen Fehler) trifft; \texttt{c} wird als \texttt{int}
und nicht als \texttt{char} deklariert, damit es gross genug ist, um den alle möglichen
Wert aufzunehmen.

\clearpage
Die Zeile:

\begin{minted}{c}
    #include <stdio.h>
\end{minted}

sollte am Anfang jeder Quelldatei stehen. Es veranlasst den C-Compiler,
eine Header-Datei (\texttt{/usr/include/stdio.h}) mit Standardroutinen
und -symbolen zu lesen, die die Definition von \texttt{EOF} enthält.
Wir werden \texttt{<stdio.h>} als Abkürzung für den vollständigen Dateinamen
im Text verwenden.

Die Datei \texttt{<ctype.h>} ist eine weitere Header-Datei in \texttt{/usr/include},
die maschinenunabhängige Tests zur Bestimmung der Eigenschaften von Zeichen definiert.
Wir haben hier \texttt{isascii} und \texttt{isprint} verwendet, um festzustellen,
ob das Eingabezeichen ASCII (d.h.\ ein Wert kleiner als \texttt{0200}) und druckbar
ist; andere Tests sind in Tabelle~\ref{tab:io-tests} aufgeführt. Beachte, dass Zeilenumbruch,
Tabulator und Leerzeichen nach den Definitionen in \texttt{<ctype.h>} nicht druckbar sind.

Der Aufruf von \texttt{exit} am Ende von \cmd{vis} ist nicht notwendig, damit das
Programm richtig funktioniert, aber er stellt sicher, dass jeder Aufrufer des
Programms einen normalen Exit-Status (üblicherweise \texttt{0}) des Programms sieht,
wenn es beendet ist. Eine andere Möglichkeit, den Status zurückzugeben, besteht darin,
\texttt{main} mit \texttt{return 0} zu verlassen; der Rückgabewert von \texttt{main}
ist der Exit-Status des Programms. Wenn es keinen expliziten Rückgabewert oder Exit
gibt, ist der Exit-Status unvorhersehbar.

Um ein C-Programm zu kompilieren, legen Sie den Quelltext in einer Datei ab, deren
Name auf \texttt{.c} endet, z. B. \texttt{vis.1.c}, kompilieren es mit \cmd{cc} und
führen dann das Ergebnis aus, das der Compiler in einer Datei namens \texttt{a.out}
hinterlässt (\texttt{a} steht für Assembler).

Normalerweise würden wir \texttt{a.out} umbenennen, sobald es funktioniert,
oder wir verwenden die \texttt{cc}-Option \texttt{-o}, um dies direkt zu tun:

\begin{minted}{sh}
    $ cc vis.1.c -o vis
\end{minted}


\begin{table}[!t]
    \begin{tabularx}{\textwidth}{p{0.3\textwidth}X}
        \headcell{Funktion}  & \headcell{Beschreibung}                                   \\
        \addlinespace
        \texttt{isalpha(c)}  & alphabetisch: \texttt{a-z} \texttt{A-Z}                   \\ \midrule
        \texttt{isupper(c)}  & Grossbuchstaben: \texttt{A-Z}                             \\ \midrule
        \texttt{islower(c)}  & Kleinbuchstaben: \texttt{A-Z}                             \\ \midrule
        \texttt{isdigit(c)}  & Zahlen: \texttt{0-9}                                      \\ \midrule
        \texttt{isxdigit(c)} & Hexadezimalziffer: \texttt{0-9} \texttt{a-f} \texttt{A-F} \\ \midrule
        \texttt{isalnum(c)}  & Alphabetisch oder numerisch                               \\ \midrule
        \texttt{isspace(c)}  & Leerzeichen, Tabulator, Zeilenumbruch oder Return         \\ \midrule
        \texttt{ispunct(c)}  & nicht alphanumerisch oder Steuerung oder Leerzeichen      \\ \midrule
        \texttt{isprint(c)}  & druckbar: beliebige Grafik                                \\ \midrule
        \texttt{iscntrl(c)}  & Steuerzeichen \texttt{0 <= c < 040}                       \\ \midrule
        \texttt{isascii(c)}  & ASCII Zeichen: \texttt{0 <= c <= 0177}                    \\ \midrule
    \end{tabularx}
    \caption{Makros für Zeichentests (\texttt{<ctype.h>})}
    \label{tab:io-tests}
\end{table}

\subsubsection{Programm-Argumente: \cmd{vis} Version 2}

Bei der Ausführung eines C-Programms werden der Funktion \texttt{main} die
Befehlszeilenargumente als Anzahl \texttt{argc} und ein Array von \texttt{argv}-Zeigern
auf Zeichenketten, die die Argumente enthalten, zur Verfügung gestellt.
Der Konvention nach ist \texttt{argv[0]} der Befehlsname selbst, so dass \texttt{argc}
immer grösser als 0 ist; die nützlichen Argumente sind \texttt{argv[1]} $\ldots$
\texttt{argv[argc-1]}. Erinnern wir uns daran, dass die Umleitung mit \texttt{<}
und \texttt{>} von der Shell durchgeführt wird, nicht von einzelnen Programmen,
so dass die Umleitung keine Auswirkungen auf die Anzahl der Argumente hat,
die das Programm sieht.

Um die Behandlung von Argumenten zu veranschaulichen, wollen wir \cmd{vis} um ein
optionales Argument erweitern: vis \texttt{-s} entfernt alle nicht druckbaren
Zeichen, anstatt sie prominent anzuzeigen. Diese Option ist nützlich, um Dateien
von anderen Systemen zu bereinigen, z. B. solche, die \texttt{CRLF}
(\emph{Carriage Return} und \emph{Line Feed}) anstelle von Newline zum Beenden
von Zeilen verwenden.

\texttt{argv} ist ein Zeiger auf ein Array, dessen einzelne Elemente Zeiger
auf Arrays von Zeichen sind; jedes Array wird durch das ASCII-Zeichen NUL (\verb|\0|)
abgeschlossen, so dass es wie eine Zeichenkette behandelt werden kann. In dieser
Version von \cmd{vis} wird zunächst geprüft, ob ein Argument vorhanden ist und
ob es sich um \texttt{-s} handelt (ungültige Argumente werden ignoriert).
Die Funktion \href{https://www.man7.org/linux/man-pages/man3/strcmp.3.html}{strcmp}
vergleicht zwei Zeichenketten und gibt Null zurück, wenn sie identisch sind.

\src{c}{src/examples/}{vis.2.c}

Tabelle~\ref{tab:string-functions} listet eine Reihe von Funktionen zur Behandlung
von Zeichenketten und allgemeinen Hilfsfunktionen auf, von denen \texttt{strcmp} eine ist.
Es ist in der Regel am besten, diese Funktionen zu verwenden, anstatt eigene zu
schreiben, da sie standardisiert sind, von Fehlern befreit sind und oft schneller
sind als das, was Sie selbst schreiben können, da sie für bestimmte Maschinen
optimiert wurden.

\begin{table}[!t]
    \begin{tabularx}{\textwidth}{p{0.15\textwidth}X}
        \headcell{Funktion}   & \headcell{Beschreibung}                                                      \\
        \addlinespace
        \texttt{strcat(s,t)}    & Zeichenkette \texttt{t} an \texttt{s} anhängen; zurückgeben von \texttt{s} \\ \midrule
        \texttt{strncat(s,t,n)} & höchstens \texttt{n} Zeichen von \texttt{t} an \texttt{s} anhängen         \\ \midrule
        \texttt{strcpy(s,t)}    & \texttt{t} nach \texttt{s} kopieren; \texttt{s} zurückgeben                \\ \midrule
        \texttt{strncpy(s,t,n)} & genau \texttt{n} Zeichen kopieren; ggf. mit \verb|\0| auffüllen            \\ \midrule
        \texttt{strcmp(s,t)}    & Vergleich von \texttt{s} und \texttt{t}, Rückgabe \texttt{<0, 0, >0} für \texttt{<, ==, >}      \\ \midrule
        \texttt{strncmp(s,t,n)} & höchstens \texttt{n} Zeichen vergleichen                                                        \\ \midrule
        \texttt{strlen(s)}      & Rückgabe der Länge von \texttt{s}                                                               \\ \midrule
        \texttt{strchr(s,c)}    & Rückgabe des Zeigers auf das erste \texttt{c} in \texttt{s}, \texttt{NULL} wenn nicht vorhanden \\ \midrule
        \texttt{atoi(s)}        & Rückgabe des ganzzahligen Wertes von \texttt{s}                                                 \\ \midrule
        \texttt{atof(s)}        & Rückgabe des Fliesskommawertes von \texttt{s}                                                   \\ \midrule
        \texttt{malloc(n)}      & Rückgabe eines Zeigers auf \texttt{n} Bytes Speicher, \texttt{NULL} wenn nicht möglich          \\ \midrule
        \texttt{calloc(n,m)}    & Rückgabezeiger auf $n \times m$ Bytes, auf \texttt{0} gesetzt, \texttt{NULL} wenn nicht möglich \\ \midrule
        \texttt{free(p)}        & durch \texttt{malloc} oder \texttt{calloc} zugewiesenen Speicher freigeben                      \\ \midrule
    \end{tabularx}
    \caption{Standard String-Funktionen}
    \label{tab:string-functions}
\end{table}


\subsubsection{Dateizugriff: \cmd{vis} Version 3}

Die ersten beiden Versionen von \texttt{vis} lesen die Standardeingabe und
schreiben die Standardausgabe, die beide von der Shell geerbt werden.
Der nächste Schritt besteht darin, \texttt{vis} so zu modifizieren, dass es
auf Dateien über deren Namen zugreift, so dass

\begin{minted}{sh}
    $ vis file file2 ...
\end{minted}

die benannten Dateien anstelle der Standardeingabe durchsucht. Wenn es jedoch
keine Argumente für den Dateinamen gibt, soll vis trotzdem die Standardeingabe lesen.

Die Frage ist, wie die Dateien gelesen werden können, d. h. wie die Dateinamen
von den E/A-Anweisungen, die die Daten tatsächlich lesen, erfasst werden können.

Die Regeln sind einfach. Bevor eine Datei gelesen oder geschrieben werden kann,
muss sie mit der Standardbibliotheksfunktion \href{https://man7.org/linux/man-pages/man3/fopen.3.html}%
{fopen} geöffnet werden. \texttt{fopen} nimmt einen Dateinamen, erledigt einige
Aufgaben und Verhandlungen mit dem Kernel und gibt einen internen Namen zurück,
der bei nachfolgenden Operationen mit der Datei verwendet wird.

Dieser interne Name ist eigentlich ein Zeiger, ein so genannter Dateizeiger, auf
eine Struktur, die Informationen über die Datei enthält, z. B. die Position
eines Puffers, die aktuelle Zeichenposition im Puffer, ob die Datei gerade
gelesen oder geschrieben wird und ähnliches. Eine der Definitionen,
die man durch Einbindung von \texttt{<stdio.h>} erhält, ist für eine Struktur
namens \texttt{FILE}. Die Deklaration für einen Dateizeiger lautet

\begin{minted}{c}
    FILE *fp;
\end{minted}

Dies besagt, dass \texttt{fp} ein Zeiger auf ein \texttt{FILE} ist. \texttt{fopen}
gibt einen Zeiger auf \texttt{FILE} zurück; die Typdeklaration für \texttt{fopen}
ist in \texttt{<stdio.h>} zu finden.

Der eigentliche Aufruf von \texttt{fopen} in einem Programm lautet:

\begin{minted}{c}
    char *name, *mode;

    fp = fopen(name, mode);
\end{minted}

Das erste Argument von \texttt{fopen} ist der Name der Datei in Form einer Zeichenkette.
Das zweite Argument, ebenfalls eine Zeichenkette, gibt an, wie Sie die Datei verwenden
wollen; die zulässigen Modi sind lesen (\texttt{r}), schreiben (\texttt{w}) oder
anhängen (\texttt{a}).

Wenn eine Datei, die Sie zum Schreiben oder Anhängen öffnen, nicht existiert, wird
sie nach Möglichkeit erstellt. Wird eine vorhandene Datei zum Schreiben geöffnet,
wird der alte Inhalt verworfen. Der Versuch, eine Datei zu lesen, die nicht existiert,
ist ein Fehler, ebenso wie der Versuch, eine Datei zu lesen oder zu schreiben,
wenn man keine Berechtigung hat. Wenn ein Fehler auftritt, gibt \texttt{fopen}
den ungültigen Zeigerwert \texttt{NULL}.

Als nächstes wird eine Möglichkeit benötigt, die Datei zu lesen oder zu schreiben,
sobald sie geöffnet ist. Es gibt mehrere Möglichkeiten, von denen 
\href{https://man7.org/linux/man-pages/man3/getc.3p.html}{getc} und 
\href{https://www.man7.org/linux/man-pages/man3/putc.3p.html}{putc} die einfachsten sind.
\texttt{getc} holt das nächste Zeichen aus einer Datei.

\begin{minted}{c}
    c = getc(fp)
\end{minted}

\texttt{putc} ist analog zu \texttt{getc}:

\begin{minted}{c}
    putc(c, fp)
\end{minted}

Wenn ein Programm gestartet wird, sind bereits drei Dateien geöffnet, für die es
Dateizeiger gibt. Diese Dateien sind die Standardeingabe, die Standardausgabe
und die Standardfehlerausgabe; die entsprechenden Dateizeiger heissen
\texttt{stdin}, \texttt{stdout} und \texttt{stderr}. Diese Dateizeiger werden
in \texttt{<stdio.h>} deklariert; sie können überall verwendet werden, wo ein
Objekt vom Typ \texttt{FILE *} sein kann. Sie sind jedoch Konstanten, keine
Variablen, so dass man ihnen nichts zuweisen kann.

\href{https://www.man7.org/linux/man-pages/man3/getchar.3p.html}{getchar} ist dasselbe
wie \texttt{getc(stdin)} und
\href{https://www.man7.org/linux/man-pages/man3/putchar.3p.html}{putchar(c)}
ist dasselbe wie \texttt{putc(c,stdout)}.
Tatsächlich sind alle vier Funktionen als Makros in \texttt{<stdio.h>} definiert,
da sie schneller laufen, weil sie den Overhead eines Funktionsaufrufs für jedes
Zeichen vermeiden. Siehe Tabelle~\ref{tab:stdio-definitions} für einige andere Definitionen in <stdio.h>.

Nachdem einige Vorarbeiten erledigt sind, können wir nun die dritte Version von \cmd{vis}
schreiben. Wenn es Befehlszeilenargumente gibt, werden sie der Reihe nach verarbeitet.
Wenn keine Argumente vorhanden sind, wird die Standardeingabe verarbeitet.

\src{c}{src/examples/}{vis.3.c}

Dieser Code stützt sich auf die Konvention, dass optionale Argumente zuerst kommen.
Nachdem jedes optionale Argument verarbeitet wurde, werden \texttt{argc} und \texttt{argv}
angepasst, so dass der Rest des Programms unabhängig vom Vorhandensein dieses Arguments ist.
Obwohl \cmd{vis} nur eine einzige Option kennt, haben wir den Code als Schleife geschrieben,
um eine Möglichkeit zu zeigen, die Verarbeitung von Argumenten zu organisieren.

Die verwendete Funktion \texttt{vis()} druckt eine einzelne Datei:

\begin{minted}{c}
    void vis(FILE *fp) {
        int c;

        while ((c = getc(fp)) != EOF)
            if (isascii(c) && (isprint(c) || c == '\n' || c == '\t' || c == ' '))
                putchar(c);
            else if (!strip)
                printf("\\%03o", c);
    }
\end{minted}

Die Funktion \texttt{fclose} trennt die Verbindung zwischen einem Dateizeiger
und dem externen Dateinamen, die durch \texttt{fopen} hergestellt wurde,
und gibt den Dateizeiger für die Verwendung mit einer anderen Datei frei.
Da die Anzahl der gleichzeitig geöffneten Dateien in einem Programm begrenzt
ist, ist es ratsam, Dateien freizugeben, wenn sie nicht mehr benötigt werden.

\begin{table}[t]
    \begin{tabularx}{\textwidth}{p{0.15\textwidth}X}
        \headcell{Funktion}   & \headcell{Beschreibung}                                         \\
        \addlinespace
        \texttt{stdin}      & Standardeingabe                                                   \\ \midrule
        \texttt{stdout}     & Standardausgabe                                                   \\ \midrule
        \texttt{stderr}     & Standardfehler                                                    \\ \midrule
        \texttt{EOF}        & Ende der Datei, normalerweise -1                                  \\ \midrule
        \texttt{NULL}       & ungültiger Zeiger, normalerweise 0                                \\ \midrule
        \texttt{FILE}       & für die Deklaration von Dateizeigern verwendet                    \\ \midrule
        \texttt{BUFSIZ}     & normale E/A-Puffergrösse (oft 512 oder 1024)                      \\ \midrule
        \texttt{getc(fp)}   & Rückgabe eines Zeichens aus dem Datenstrom \texttt{fp}            \\ \midrule
        \texttt{getchar()}  & \texttt{getc(stdin)}                                              \\ \midrule
        \texttt{putc(c,fp)} & Zeichen \texttt{c} auf den Datenstrom \texttt{fp} legen           \\ \midrule
        \texttt{putchar(c)} & \texttt{putc(c,stdout)}                                           \\ \midrule
        \texttt{feof(fp)}   & Nicht-Null, wenn Ende der Datei im Datenstrom \texttt{fp}         \\ \midrule
        \texttt{ferror(fp)} & ungleich Null, wenn ein Fehler im Datenstrom \texttt{fp} auftritt \\ \midrule
        \texttt{fileno(fp)} & Dateideskriptor für Datenstrom \texttt{fp}                        \\ \midrule
    \end{tabularx}
    \caption{Standard String-Funktionen}
    \label{tab:stdio-definitions}
\end{table}

Ausgaben, die mit Standardbibliotheksfunktionen wie \texttt{printf} oder \texttt{putc}
erzeugt werden, sind normalerweise gepuffert, um effizienter in grossen Blöcken
geschrieben zu werden. Eine Ausnahme bildet die Ausgabe an ein Terminal, die in
der Regel sofort geschrieben wird, insbesondere bei einem Zeilenumbruch.
Der Aufruf von \texttt{fclose} für eine Ausgabedatei erzwingt ebenfalls das
Schreiben der gepufferten Daten. \texttt{fclose} wird ausserdem automatisch für
jede geöffnete Datei aufgerufen, wenn ein Programm \texttt{exit} aufruft oder
die \texttt{main}-Funktion beendet wird.

\texttt{stderr} wird einem Programm genauso wie \texttt{stdin} und
\texttt{stdout} zugewiesen. Ausgaben, die auf \texttt{stderr} geschrieben
werden, erscheinen auf dem Terminal des Benutzers, selbst wenn die
Standardausgabe umgeleitet wird. Das Programm \cmd{vis} schreibt seine
Fehlermeldungen auf \texttt{stderr} anstatt auf \texttt{stdout}, sodass
Meldungen bei Zugriffsproblemen auf Dateien direkt auf dem Terminal
erscheinen und nicht in einer Pipeline oder Ausgabedatei verloren gehen.
Dies ist eine Folge davon, dass der Standardfehler erst nach den Pipes
eingeführt wurde, um das Verschwinden von Fehlermeldungen in Pipelines
zu verhindern.

Das Programm \cmd{vis} wurde so gestaltet, dass es beendet wird, wenn
eine Eingabedatei nicht geöffnet werden kann, was für ein meist
interaktiv und mit einer einzigen Eingabedatei arbeitendes Programm
sinnvoll ist. Es gibt jedoch auch Argumente für eine alternative Gestaltung.

% =======
\input{chapters/06io-exercises02}
% =======
