#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    pid_t pid = fork();

    if (pid == -1) {
        // Fehler beim Erstellen des Prozesses
        perror("fork failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Kindprozess
        printf("Ich bin der Kindprozess.\n");
    } else {
        // Elternprozess
        wait(NULL); // Warten auf Beendigung des Kindprozesses
        printf("Ich bin der Elternprozess.\n");
    }

    return EXIT_SUCCESS;
}