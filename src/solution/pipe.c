#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main() {
    int pipefd[2];
    pid_t pid;
    char buf;
    char *message = "Hallo vom Elternprozess!\n";

    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) { // Kindprozess
        close(pipefd[1]); // Schreibseite der Pipe schließen

        while (read(pipefd[0], &buf, 1) > 0) {
            write(STDOUT_FILENO, &buf, 1);
        }

        write(STDOUT_FILENO, "\n", 1);
        close(pipefd[0]);
        exit(EXIT_SUCCESS);
    } else { // Elternprozess
        close(pipefd[0]); // Leseseite der Pipe schließen
        write(pipefd[1], message, strlen(message));
        close(pipefd[1]); // Schreibseite schließen, um EOF zu senden
        wait(NULL); // Warten auf Beendigung des Kindprozesses
    }

    return EXIT_SUCCESS;
}