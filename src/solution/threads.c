#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *printMessage(void *threadId) {
    printf("Thread %ld: Hallo Welt!\n", (long)threadId);
    pthread_exit(NULL);
}

int main() {
    pthread_t threads[2];

    for (long i = 0; i < 2; i++) {
        int rc = pthread_create(&threads[i], NULL, printMessage, (void *)i);
        if (rc) {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    pthread_exit(NULL); // Warten auf Beendigung aller Threads
}