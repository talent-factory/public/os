#include <pthread.h>
#include <stdio.h>

int counter = 0;
pthread_mutex_t lock;

void *incrementCounter(void *threadId) {
    pthread_mutex_lock(&lock);
    counter++;
    printf("Thread %ld hat den Zähler erhöht. Neuer Wert: %d\n", (long)threadId, counter);
    pthread_mutex_unlock(&lock);
    pthread_exit(NULL);
}

int main() {
    pthread_t threads[2];
    pthread_mutex_init(&lock, NULL);

    for (long i = 0; i < 2; i++) {
        pthread_create(&threads[i], NULL, incrementCounter, (void *)i);
    }

    for (int i = 0; i < 2; i++) {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&lock);
    pthread_exit(NULL);
}