#include <stdio.h>
#include <unistd.h>

/* cat:  minimal version (version 1)*/
int main() {

    char buf[BUFSIZ]; // defined in <stdio.h>
    long n;

    while ((n = read(0, buf, sizeof(buf))) > 0)
        write(1, buf, n);

    return 0;
}
