#include <stdio.h>
#include <stdlib.h>

int main() {
    int n;
    int *arr;

    // Benutzereingabe für die Grösse des Arrays
    printf("Geben Sie die Grösse des Arrays ein: ");
    scanf("%d", &n);

    // Dynamische Speicherzuweisung für das Array
    arr = (int *)malloc(n * sizeof(int));

    // Überprüfen, ob die Speicherzuweisung erfolgreich war
    if (arr == NULL) {
        printf("Fehler: Speicher konnte nicht zugewiesen werden\n");
        return 1; // Rückgabewert 1 signalisiert einen Fehler
    }

    // Benutzereingabe für die Elemente des Arrays
    printf("Geben Sie die Elemente des Arrays ein:\n");
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    // Ausgabe der Elemente des Arrays
    printf("Das Array enthält die folgenden Elemente:\n");
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    // Speicher freigeben, wenn er nicht mehr benötigt wird
    free(arr);

    return 0; // Rückgabewert 0 signalisiert erfolgreiche Ausführung
}
