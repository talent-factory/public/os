#include <stdio.h>
#include <math.h>

int main() {
    int zahl;

    // Aufforderung zur Eingabe einer Zahl
    printf("Bitte geben Sie eine Integerzahl ein: ");
    scanf("%d", &zahl);

    // Kopfzeile der Tabelle ausgeben
    printf("\n Zahl | Quadratwurzel\n");
    printf("--------------------\n");

    // Berechnung und Ausgabe der Quadratwurzeln für die eingegebene Zahl und ihre vier Nachfolger
    for(int i = 0; i < 5; i++) {
        printf("%5d | %12.2f\n", zahl + i, sqrt(zahl + i));
    }

    return 0;
}