#include <stdio.h>

int main() {
    int zahl1, zahl2;
    float ergebnisOhneCasting, ergebnisMitCasting;

    // Eingabeaufforderung
    printf("Bitte geben Sie die erste Integerzahl ein: ");
    scanf("%d", &zahl1);
    printf("Bitte geben Sie die zweite Integerzahl ein: ");
    scanf("%d", &zahl2);

    // Überprüfung auf Division durch 0
    if (zahl2 == 0) {
        printf("Division durch Null ist nicht erlaubt.\n");
        return 1; // Beendet das Programm frühzeitig
    }

    // Division ohne erzwungene Typumwandlung
    ergebnisOhneCasting = zahl1 / zahl2;

    // Division mit erzwungener Typumwandlung
    ergebnisMitCasting = (float)zahl1 / zahl2;

    // Ausgabe der Ergebnisse
    printf("Ergebnis ohne erzwungene Typumwandlung: %f\n", ergebnisOhneCasting);
    printf("Ergebnis mit erzwungener Typumwandlung: %f\n", ergebnisMitCasting);

    return 0;
}