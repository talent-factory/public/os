#include <stdio.h>

int main() {
    int zahl1, zahl2; // Deklaration von zwei int Variablen
    int summe, differenz, produkt;
    float quotient; // Verwendung von float, um Dezimalzahlen beim Dividieren zu unterstützen

    // Aufforderung zur Eingabe der ersten Zahl
    printf("Bitte geben Sie die erste Zahl ein: ");
    scanf("%d", &zahl1);

    // Aufforderung zur Eingabe der zweiten Zahl
    printf("Bitte geben Sie die zweite Zahl ein: ");
    scanf("%d", &zahl2);

    // Berechnung der Summe, Differenz und des Produkts
    summe = zahl1 + zahl2;
    differenz = zahl1 - zahl2;
    produkt = zahl1 * zahl2;

    // Berechnung des Quotienten, mit Überprüfung um Division durch Null zu vermeiden
    if (zahl2 != 0) {
        quotient = (float)zahl1 / zahl2; // Casting zahl1 zu float für genaue Berechnung
    } else {
        printf("Division durch Null ist nicht erlaubt.\n");
        return 1; // Beendet das Programm frühzeitig mit Fehlercode 1
    }

    // Ausgabe der Ergebnisse
    printf("Summe: %5d\n", summe);
    printf("Differenz: %d\n", differenz);
    printf("Produkt: %d\n", produkt);
    printf("Quotient: %.2f\n", quotient); // Ausgabe des Quotienten mit zwei Nachkommastellen

    return 0; // Beendet das Programm erfolgreich
}