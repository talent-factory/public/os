#include <stdlib.h>
#include <unistd.h>

int main() {

    int status;

    if (fork() == 0)
        execlp("date", "date", (char *)0); /* child  */

    wait(&status); /* parent */
}