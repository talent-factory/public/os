#pragma ide diagnostic ignored "cert-err34-c"

#include <stdlib.h>
#include <stdio.h>

#define SIZE 100000
static int array[SIZE];

static int compare_int(const void *left, const void *right) {
	return (*(int *)left - *(int *)right);
}

int main(void) {
	FILE *fp;
	int i;

	fp = fopen("./random.txt", "r");
	for (i = 0; i < SIZE; i++)
		fscanf(fp, "%d", &array[i]);
	fclose(fp);

	fp = fopen("./sort.txt", "w");

	qsort(array, SIZE, sizeof(int), compare_int);

	for (i = 0; i < SIZE; i++)
		fprintf(fp, "%d\n", array[i]);

	fclose(fp);
	return 0;
}