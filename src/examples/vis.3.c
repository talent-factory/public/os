#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int strip = 0;

void vis(FILE *fp) {
    int c;

    while ((c = getc(fp)) != EOF)
        if (isascii(c) && (isprint(c) || c == '\n' || c == '\t' || c == ' '))
            putchar(c);
        else if (!strip)
            printf("\\%03o", c);
}

int main(int argc, char *argv[]) {
    int c;

    while ((c = getopt(argc, argv, "s")) != -1)
        switch (c) {
            case 's':strip = 1;
                break;

            case '?':
                if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                return 1;

            default:abort();
        }

    if (argc == 1) {
        vis(stdin);
    } else {
        FILE *fp;
        for (int index = optind; index < argc; index++) {
            if ((fp = fopen(argv[index], "r")) == NULL) {
                fprintf(stderr, "%s: can't open %s\n", argv[0], argv[index]);
                return 1;
            } else {
                vis(fp);
                fclose(fp);
            }
        }
    }

    return 0;
}