#include <stdio.h>
#include <stdlib.h>

#define POOL_SIZE 1000 // Grösse des Memory Pools

// Struktur für einen Speicherblock im Pool
typedef struct {
  size_t size;    // Größe des Speicherblocks
  void *data;     // Zeiger auf den Speicherblock
} Block;

// Struktur für den Memory Pool
typedef struct {
  Block blocks[POOL_SIZE]; // Array von Speicherblöcken
  int count;               // Anzahl der verwendeten Speicherblöcke
} MemoryPool;

// Funktion zum Initialisieren des Memory Pools
void initPool(MemoryPool *pool) {
    pool->count = 0;
}

// Funktion zum Zuweisen von Speicher aus dem Memory Pool
void *poolAlloc(MemoryPool *pool, size_t size) {
    if (pool->count < POOL_SIZE) {
        void *data = malloc(size);
        if (data != NULL) {
            pool->blocks[pool->count].size = size;
            pool->blocks[pool->count].data = data;
            pool->count++;
            return data;
        }
    }
    return NULL; // Rückgabe NULL bei Fehler oder wenn der Pool voll ist
}

// Funktion zum Freigeben von Speicher im Memory Pool
void poolFree(MemoryPool *pool) {
    for (int i = 0; i < pool->count; i++) {
        free(pool->blocks[i].data);
    }
    pool->count = 0;
}

// Beispielanwendung
int main() {
    MemoryPool pool;
    initPool(&pool);

    // Beispielnutzung des Memory Pools
    int *ptr1 = (int *)poolAlloc(&pool, sizeof(int));
    *ptr1 = 10;

    char *ptr2 = (char *)poolAlloc(&pool, sizeof(char));
    *ptr2 = 'A';

    // Ausgabe der zugewiesenen Werte
    printf("Value of ptr1: %d\n", *ptr1);
    printf("Value of ptr2: %c\n", *ptr2);

    // Freigabe des Memory Pools
    poolFree(&pool);

    return 0;
}
