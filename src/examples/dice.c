#include <stdio.h>
#include <math.h>

int main() {
    double a; // Kantenlänge des Würfels
    double flacheninhalt, volumen, raumdiagonale;

    // Eingabeaufforderung für die Kantenlänge a
    printf("Bitte geben Sie die Kantenlänge a des Würfels ein: ");
    scanf("%lf", &a);

    // Berechnung des Flächeninhalts
    flacheninhalt = 6 * a * a;

    // Berechnung des Volumens
    volumen = a * a * a;

    // Berechnung der Länge der Raumdiagonale
    raumdiagonale = a * sqrt(3);

    // Ausgabe der Ergebnisse
    printf("Flächeninhalt: %.2f\n", flacheninhalt);
    printf("Volumen: %.2f\n", volumen);
    printf("Länge der Raumdiagonale: %.2f\n", raumdiagonale);

    return 0;
}