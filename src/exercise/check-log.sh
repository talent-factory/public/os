#!/usr/bin/env bash
# Monitoring a log file for new entries containing the word "ERROR"

tail -f system.log | grep --line-buffered "ERROR"