#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

// Kann zur Ausgabe einer Meldung verwendet werden.
void* printMessage(void* threadId) {
    printf("Thread %ld: Hallo Welt!\n", (long)threadId);
    pthread_exit(NULL);
}

int main() {
    pthread_t threads[2];

    // Hier kommt mein Code.
}