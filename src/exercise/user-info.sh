#!/usr/bin/env bash
# Asking for user information and storing it

echo -n "Geben Sie Ihren Namen ein: "
read name
echo -n "Geben Sie Ihr Geburtsjahr ein: "
read birthyear

echo "Name: $name" | tee user_info.txt
echo "Geburtsjahr: $birthyear" | tee -a user_info.txt