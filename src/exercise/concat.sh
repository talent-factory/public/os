#!/usr/bin/env bash
# Merge files while removing empty lines and sorting content

cat file1.txt file2.txt file3.txt | grep -v '^$' | sort > merged.txt
