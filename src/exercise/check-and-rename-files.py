"""
Erstellen Sie ein Skript, das eine Liste von Dateien in einem Verzeichnis erstellt.
Die Dateinamen sollten dabei bestimmten Regeln folgen (z.B. maximal 255 Zeichen,
Verwendung von Buchstaben, Zahlen und bestimmten Sonderzeichen).
Prüfen Sie, ob die Dateinamen den Regeln entsprechen und benennen
Sie sie gegebenenfalls um.
"""

import os
import re

PATTERN = '[^\w\-. ]'


def is_valid_filename(filename):
    return len(filename) <= 255 and re.match(r'^[\w\-. ]+$', filename)


def rename_files(directory):
    for root, _, files in os.walk(directory):
        for filename in files:
            if not is_valid_filename(filename):
                new_filename = re.sub(r'[^\w\-. ]', '_', filename)[:255]
                os.rename(os.path.join(root, filename), os.path.join(root, new_filename))
                print(f'Renamed: {filename} -> {new_filename}')


directory = 'your_directory_path'
rename_files(directory)
