#!/usr/bin/env bash
# Filtering lines containing a specific word and counting them

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <word>"
    exit 1
fi

word=$1
count=$(grep -c "$word" data.txt)

echo "Anzahl der Zeilen, die '$word' enthalten: $count"